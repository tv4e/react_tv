import query_string from 'query-string';
import Auth from '../auth';
import {errors, errorCode} from './Errors';
import axios from 'axios';
import retry from 'axios-retry';

//ACTION CREATORS
export function boxSuccess(response) {
  return {
    type: 'BOX_SUCCESS',
    response
  };
}

export function boxHasErrored(bool) {
  return {
    type: 'BOX_HAS_ERRORED',
    hasErrored: bool
  };
}

export function boxIsLoading(bool) {
  return {
    type: 'BOX_IS_LOADING',
    isLoading: bool
  };
}

//ACTION CALLS
export function box() {
  retry(axios, { retries: 3 });

  return (dispatch) => {
    dispatch(boxIsLoading(true));
    axios({
      method: 'get',
      url: `http://api_mysql.tv4e.pt/api/boxes/box/${Auth.getId()}`,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': 'application/x-www-form-urlencoded',
        'X-Requested-With': 'XMLHttpRequest',
      },
    })
      .then((response) => {
        if (response.status != 200) {
          console.log('error');
          throw Error(response.statusText);
        }

        Auth.setBox(response.data);
        dispatch(boxIsLoading(false));
        dispatch(boxSuccess(response.data));
      })
      .catch(() => dispatch(boxHasErrored(true)));
  };
}

