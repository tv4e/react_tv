import axios from 'axios';
import query_string from 'query-string';
import Auth from '../auth';
import {errors, errorCode} from './Errors';
import retry from 'axios-retry';


//ACTION CREATORS
export function cronSuccess(response) {
  return {
    type: 'CRON_SUCCESS',
    response
  };
}

export function cronHasErrored(bool) {
  return {
    type: 'CRON_HAS_ERRORED',
    hasErrored: bool
  };
}
export function cronIsLoading(bool) {
  return {
    type: 'CRON_IS_LOADING',
    hasErrored: bool
  };
}

//ACTION CALLS
export function cron() {
  return (dispatch) => {
    dispatch(cronIsLoading(true));
    axios({
      method: 'get',
      url: `http://api_mysql.tv4e.pt/api/boxes/getvideo/${Auth.getId()}`,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': 'application/x-www-form-urlencoded',
        'X-Requested-With': 'XMLHttpRequest'
      }
    })
      .then((response) => {
        if (response.status != 200) {
          console.log('error');
          throw Error(response.statusText);
        }

        dispatch(cronIsLoading(false));
        dispatch(cronSuccess(response));
      })
      .catch(() => {
        dispatch(cronHasErrored(true));
      });
  };
}