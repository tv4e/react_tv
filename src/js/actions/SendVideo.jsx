'use strict';

import query_string from 'query-string';
import Auth from '../auth';
import {errors, errorCode} from './Errors';
import axios from 'axios';
import retry from 'axios-retry';

//ACTION CREATORS
export function sendVideoSuccess(response) {
  return {
    type: 'SENDVIDEO_SUCCESS',
    response
  };
}

export function sendVideoHasErrored(bool) {
  return {
    type: 'SENDVIDEO_HAS_ERRORED',
    hasErrored: bool
  };
}

export function sendVideoIsLoading(bool) {
  return {
    type: 'SENDVIDEO_IS_LOADING',
    isLoading: bool
  };
}

//ACTION CALLS
export function sendVideo() {
      retry(axios, { retries: 3 });

  return (dispatch) => {
    dispatch(sendVideoIsLoading(true));
    axios({
      method: 'post',
      url: `http://api_mysql.tv4e.pt/api/boxes/sendvideo/${Auth.getId()}`,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': 'application/x-www-form-urlencoded',
        'X-Requested-With': 'XMLHttpRequest',
      },
    })
      .then((response) => {
        if (response.status != 200) {
          console.log('error');
          throw Error(response.statusText);
        }
        dispatch(sendVideoIsLoading(false));
        dispatch(sendVideoSuccess(response.data));
      })
      .catch(() => dispatch(sendVideoHasErrored(true)));
  };
}

