'use strict';

import query_string from 'query-string';
import Auth from '../auth';
import {errors, errorCode} from './Errors';
import axios from 'axios';
import retry from 'axios-retry';

//ACTION CREATORS
export function taxiSuccess(response) {
  return {
    type: 'TAXI_SUCCESS',
    response
  };
}

export function taxiHasErrored(bool) {
  return {
    type: 'TAXI_HAS_ERRORED',
    hasErrored: bool
  };
}

export function taxiIsLoading(bool) {
  return {
    type: 'TAXI_IS_LOADING',
    isLoading: bool
  };
}

//ACTION CALLS
export function taxis() {
  retry(axios, { retries: 3 });
  return (dispatch) => {
    dispatch(taxiIsLoading(true));
    axios({
      method: 'get',
      url: `http://api_mysql.tv4e.pt/api/boxes/taxis/${Auth.getId()}`,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': 'application/x-www-form-urlencoded',
        'X-Requested-With': 'XMLHttpRequest',
      },
    })
      .then((response) => {
        if (response.status != 200) {
          console.log('error');
          throw Error(response.statusText);
        }
        dispatch(taxiIsLoading(false));
        dispatch(taxiSuccess(response.data));
      })
      .catch(() => dispatch(taxiHasErrored(true)));
  };
}

