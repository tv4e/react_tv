import axios from 'axios';
import query_string from 'query-string';
import Auth from '../auth';
import {errors, errorCode} from './Errors';
import retry from 'axios-retry';

//ACTION CREATORS
export function ratingsSuccess(response) {
  return {
    type: 'RATINGS_SUCCESS',
    response
  };
}

export function ratingsIsLoading(bool) {
  return {
    type: 'RATINGS_IS_LOADING',
    isLoading: bool
  };
}

export function ratingsHasErrored(bool) {
  return {
    type: 'RATINGS_HAS_ERRORED',
    hasErrored: bool
  };
}

//ACTION CALLS
export function ratings(payload) {
  retry(axios, { retries: 3 });

  return (dispatch) => {
    dispatch(ratingsIsLoading(true));
    axios({
      method: 'post',
      url: `http://api_mysql.tv4e.pt/api/boxes/rating/${Auth.getId()}`,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': 'application/x-www-form-urlencoded',
        'X-Requested-With': 'XMLHttpRequest'
      },
      data: query_string.stringify(payload)
    })
      .then((response) => {
        if (response.status != 200) {
          console.log('error');
          throw Error(response.statusText);
        }
        dispatch(ratingsIsLoading(false));
        dispatch(ratingsSuccess(response));
      })
      .catch(() => {
        dispatch(ratingsHasErrored(true));
      });
  };
}