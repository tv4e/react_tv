'use strict';

import query_string from 'query-string';
import Auth from '../auth';
import {errors, errorCode} from './Errors';
import axios from 'axios';
import retry from 'axios-retry';

//ACTION CREATORS
export function farmaciaSuccess(response) {
  return {
    type: 'FARMACIA_SUCCESS',
    response
  };
}

export function farmaciaHasErrored(bool) {
  return {
    type: 'FARMACIA_HAS_ERRORED',
    hasErrored: bool
  };
}

export function farmaciaIsLoading(bool) {
  return {
    type: 'FARMACIA_IS_LOADING',
    isLoading: bool
  };
}

//ACTION CALLS
export function farmacias() {
  retry(axios, { retries: 3 });

  return (dispatch) => {
    dispatch(farmaciaIsLoading(true));
    axios({
      method: 'get',
      url: `http://api_mysql.tv4e.pt/api/boxes/farmacias/${Auth.getId()}`,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': 'application/x-www-form-urlencoded',
        'X-Requested-With': 'XMLHttpRequest',
      },
    })
      .then((response) => {
        if (response.status != 200) {
          console.log('error farmacias');
          throw Error(response.statusText);
        }

        dispatch(farmaciaIsLoading(false));
        dispatch(farmaciaSuccess(response.data));
      })
      .catch(() => dispatch(farmaciaHasErrored(true)));
  };
}

