'use strict';
import {store} from '../index.jsx';
import axios from 'axios';
import query_string from 'query-string';
import Auth from '../auth';
import retry from 'axios-retry';

//ACTION CREATORS
export function socket(response, listener) {
  response.listener = listener;
  return {
    type: 'SOCKET',
    response
  };
}

//ACTION CALLS
export function socketListener(data, listener) {
  store.dispatch(socket(data, listener));
}

//ACTION CREATORS
export function socketEmitSuccess(response, listener) {
  response.listener = listener;
  return {
    type: 'SOCKET_EMIT_SUCCESS',
    response
  };
}

export function socketEmitHasErrored(bool) {
  return {
    type: 'SOCKET_EMIT_HAS_ERRORED',
    bool
  };
}

export function socketEmitIsLoading(bool) {
  return {
    type: 'SOCKET_EMIT_IS_LOADING',
    isLoading: bool
  };
}

//ACTION CALLS
export function socketEmit(payload, event) {

  console.log(payload);
  console.log(event);
  retry(axios, { retries: 3 });

  return (dispatch) => {
    dispatch(socketEmitIsLoading(true));
    axios({
      method: 'post',
      url: `http://api_mysql.tv4e.pt/api/socket/${event}/${Auth.getId()}`,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': 'application/x-www-form-urlencoded',
        'X-Requested-With': 'XMLHttpRequest'
      },
      data: query_string.stringify(payload)
    })
      .then((response) => {
        if (response.status != 200) {
          console.log('error');
          throw Error(response.statusText);
        }

        dispatch(socketEmitSuccess(response));
      })
      .catch(() => dispatch(socketEmitHasErrored(true)));
  };
}
