import * as Error from './general/Errors';
import * as Socket from './general/Socket';
import * as Profile from './general/Profile';
import * as Logs from './general/Logs';
import * as Weather from './general/Weather';
import * as Farmacias from './general/Farmacias';
import * as SendVideo from './general/SendVideo';
import * as Taxis from './general/Taxis';
import * as Box from './general/Box';
import * as Cron from './general/Cron';

import _ from 'lodash';

let reducers = _.extend({},
  Error,
  Profile,
  Weather,
  Logs,
  Farmacias,
  Taxis,
  SendVideo,
  Box,
  Cron,
  Socket
);

module.exports = reducers;

