'use strict';

export function socket(state = [], action) {
  switch (action.type) {
    case 'SOCKET':
      switch (Object.keys(action.response)[0]) {
        case 'box':
          return {
            [action.response[Object.keys(action.response)[1]]]:
              action.response[Object.keys(action.response)[0]]
          };
      }
      break;
    default:
      return state;
  }
}

// SOCKET_EMIT.........................................................................

export function socketEmitSuccess(state = [], action) {
  switch (action.type) {
    case 'SOCKET_EMIT_SUCCESS':
      return action.response;
      break;
    default:
      return state;
  }
}

export function socketEmitHasErrored(state = false, action) {
  switch (action.type) {
    case 'SOCKET_EMIT_HAS_ERRORED':
      return action.hasErrored;
      break;
    default:
      return state;
  }
}

export function socketEmitIsLoading(state = false, action) {
  switch (action.type) {
    case 'SOCKET_EMIT_IS_LOADING':
      return action.isLoading;
      break;
    default:
      return state;
  }
}