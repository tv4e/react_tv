'use strict';

export function boxSuccess(state = [], action) {
  switch (action.type) {
    case 'BOX_SUCCESS':
      return action.response;
      break;
    default:
      return state;
  }
}

export function boxIsLoading(state = false, action) {
  switch (action.type) {
    case 'BOX_IS_LOADING':
      return action.isLoading;
      break;
    default:
      return state;
  }
}

export function boxHasErrored(state = false, action) {
  switch (action.type) {
    case 'BOX_HAS_ERRORED':
      return action.hasErrored;
      break;
    default:
      return state;
  }
}