'use strict';

export function logSuccess(state = [], action) {
  switch (action.type) {
    case 'LOG_SUCCESS':
      return action.response;
      break;
    default:
      return state;
  }
}

export function logHasErrored(state = false, action) {
  switch (action.type) {
    case 'LOG_HAS_ERRORED':
      return action.hasErrored;
      break;
    default:
      return state;
  }
}

//--------------------------------------------------------\\
//----------------------------------------------------------\\

export function channelSuccess(state = [], action) {
  switch (action.type) {
    case 'CHANNEL_SUCCESS':
      return action.response;
      break;
    default:
      return state;
  }
}

export function channelLoading(state = false, action) {
  switch (action.type) {
    case 'CHANNEL_IS_LOADING':
      return action.isLoading;
      break;
    default:
      return state;
  }
}

export function channelHasErrored(state = false, action) {
  switch (action.type) {
    case 'CHANNEL_HAS_ERRORED':
      return action.hasErrored;
      break;
    default:
      return state;
  }
}