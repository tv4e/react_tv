
export function ratingsSuccess(state = [], action) {
  switch (action.type) {
    case 'RATINGS_SUCCESS':
      return action.response;
      break;
    default:
      return state;
  }
}

export function ratingsIsLoading(state = false, action) {
  switch (action.type) {
    case 'RATINGS_IS_LOADING':
      return action.isLoading;
      break;
    default:
      return state;
  }
}

export function ratingsHasErrored(state = false, action) {
  switch (action.type) {
    case 'RATINGS_HAS_ERRORED':
      return action.hasErrored;
      break;
    default:
      return state;
  }
}
