export function cronSuccess(state = [], action) {
  switch (action.type) {
    case 'CRON_SUCCESS':
      return action.response;
      break;
    default:
      return state;
  }
}

export function cronHasErrored(state = false, action) {
  switch (action.type) {
    case 'CRON_HAS_ERRORED':
      return action.hasErrored;
      break;
    default:
      return state;
  }
}

export function cronIsLoasing(state = false, action) {
  switch (action.type) {
    case 'CRON_IS_LOADING':
      return action.hasErrored;
      break;
    default:
      return state;
  }
}