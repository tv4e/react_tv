'use strict';

export function sendVideoSuccess(state = [], action) {
  switch (action.type) {
    case 'SENDVIDEO_SUCCESS':
      return action.response;
      break;
    default:
      return state;
  }
}

export function sendVideoIsLoading(state = false, action) {
  switch (action.type) {
    case 'SENDVIDEO_IS_LOADING':
      return action.isLoading;
      break;
    default:
      return state;
  }
}

export function sendVideoHasErrored(state = false, action) {
  switch (action.type) {
    case 'SENDVIDEO_HAS_ERRORED':
      return action.hasErrored;
      break;
    default:
      return state;
  }
}