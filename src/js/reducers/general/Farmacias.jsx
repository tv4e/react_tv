'use strict';

export function farmaciaSuccess(state = [], action) {
  switch (action.type) {
    case 'FARMACIA_SUCCESS':
      return action.response;
      break;
    default:
      return state;
  }
}

export function farmaciaIsLoading(state = false, action) {
  switch (action.type) {
    case 'FARMACIA_IS_LOADING':
      return action.isLoading;
      break;
    default:
      return state;
  }
}

export function farmaciaHasErrored(state = false, action) {
  switch (action.type) {
    case 'FARMACIA_HAS_ERRORED':
      return action.hasErrored;
      break;
    default:
      return state;
  }
}