'use strict';

export function taxiSuccess(state = [], action) {
  switch (action.type) {
    case 'TAXI_SUCCESS':
      return action.response;
      break;
    default:
      return state;
  }
}

export function taxiIsLoading(state = false, action) {
  switch (action.type) {
    case 'TAXI_IS_LOADING':
      return action.isLoading;
      break;
    default:
      return state;
  }
}

export function taxiHasErrored(state = false, action) {
  switch (action.type) {
    case 'TAXI_HAS_ERRORED':
      return action.hasErrored;
      break;
    default:
      return state;
  }
}