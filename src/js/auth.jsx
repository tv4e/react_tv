'use strict';

module.exports = {

  setToken(token) {
    localStorage.token = token;
    return localStorage.token;
  },

  getToken() {
    return localStorage.token;
  },

  setId(id) {
    localStorage.id = id;
    return localStorage.id;
  },

  getId() {
    return localStorage.id;
  },

  setIptv(iptv) {
    localStorage.iptv = iptv;
    return localStorage.iptv;
  },

  getIptv() {
    return localStorage.iptv;
  },

  setBox(box) {
    localStorage.box = JSON.stringify(box);
  },

  getBox() {
    return JSON.parse(localStorage.box);
  },

  logout() {
    delete localStorage.token;
    delete localStorage.id;
  },

  loggedIn() {
    //@TODO checK if hasn't expired and then log in
    return !!localStorage.token;
  },

};