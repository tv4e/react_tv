import React from 'react';
import ReactDom from 'react-dom';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';

import _ from 'lodash';

import {Provider} from 'react-redux';
import Store from './store/Store';
import {box} from './actions/Box';

import Root from './containers/Root';
import Iptv from './containers/Iptv';
import Iptv2 from './containers/Iptv2';
import IptvTeste from './containers/IptvTeste';
import VideoLibrary from './containers/VideoLibrary';
import VideoLibrary2 from './containers/VideoLibrary2';
import Rating1 from './components/Ratings/Rating1';
import Rating2 from './containers/Rating2';

import Auth from './auth';

require('../styles/global.scss');
export const store = Store();

ReactDom.render(
  /**
  * The "Provider" is a redux component to attach the store to the application
  */
  <Provider store={store}>
    <Router>
      <Root>
        <Switch>
          <Route path="/iptv" component={Iptv}/>
          <Route path="/iptv2" component={Iptv2}/>
          <Route path="/iptv_teste" component={IptvTeste}/>
          <Route path="/library" component={VideoLibrary}/>
          <Route path="/library2" component={VideoLibrary2}/>
          <Route path="/rating1" component={Rating1}/>
          <Route path="/rating2" component={Rating2}/>
        </Switch>
      </Root>
    </Router>
  </Provider>,
  document.getElementById('root')
);

function _checkAuth() {
  // if (Auth.loggedIn()) {
  //     browserHistory.push('/');
  // }
}

