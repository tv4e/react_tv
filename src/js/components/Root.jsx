import React from 'react';
import isOnline from 'is-online';
import Auth from '../auth';
import _ from 'lodash';
import Socket from '../Socket';

/**
* Root
* This component sets the initial layout after making sure there is internet and sockect connection are stabilished
*/
export default class Root extends React.Component {
  constructor(props) {
    super(props);

    if (!_.isEmpty(this._getURLParameter('id')) && this._getURLParameter('id') != Auth.getId() || Auth.getId() == 'null' || Auth.getId() == undefined || _.isEmpty(Auth.getId())) {
      Auth.setId(this._getURLParameter('id'));
    }

    Socket.connect(`box.${Auth.getId()}`, `SendVideos`);
    Socket.connect(`box.${Auth.getId()}`, `SendKey`);
    Socket.connect(`box.${Auth.getId()}`, `AppReady`);
    Socket.connect(`box.${Auth.getId()}`, `SendHdmiState`);
    Socket.connect(`box.${Auth.getId()}`, `RefreshLib`);
  }

  componentWillMount() {

    this.props.actionCreators.box();
    setInterval(function () {
      isOnline().then(online => {
        if (online == true) {
          this.props.actionCreators.internet(true);
        } else {
          this.props.actionCreators.internet(false);
        }
      });
    }.bind(this), 1000);
  };

  componentWillReceiveProps(nextProps) {
    if (!_.isEmpty(nextProps.actionData.box.items) && (!_.isEqual(this.props.actionData.box.items, nextProps.actionData.box.items))) {
      this.props = nextProps;
      Auth.setIptv(this.props.actionData.box.items.iptv_type);
      this.props.history.push(`/${Auth.getIptv()}`);
    }
  }

  render() {
    return (
      <section className="mainLayout">
        {this.props.children}
      </section>
    );
  };

  _getURLParameter(name) {
    return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search) || [null, ''])[1].replace(/\+/g, '%20')) || null;
  }
};
