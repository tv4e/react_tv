import React from 'react';

/**
* Rating screen shown as an overlay
* As a timer and asks users if they likeed a video showing a thumbs up and a thumbs down
* requires the informative video id make a POST request with the rating values
*/
export default class Rating extends React.Component {

    constructor(props) {
        super(props);

        this.videoID = props.videoID;
        this.close = props.closeRating;

        this._handleKey = this._handleKey.bind(this);

        this.intervalRating = "";

        this.state = {
            rating: 0,
            timer:30
        };
    }

    render() {
        let likeOn = "fa fa-thumbs-up large green";
        let likeOff = "fa fa-thumbs-up large";
        let dislikeOn = "fa fa-thumbs-down red";
        let dislikeOff = "fa fa-thumbs-down";

        return (
            <section className="mainLayout">
              <div className="mainLayout-wrapper ">
                <div className="overlay">
                  <div className="splashScreen">
                    <div className="rating">
                      <div className="message1">
                        <p>
                          Gostou deste vídeo?
                        </p>
                      </div>
                      <div className="message2">
                          <p>
                              Gostaríamos de saber a sua opinião sobre
                              este vídeo para lhe recomendar conteúdos
                              mais adequados aos seus interesses.
                          </p>
                      </div>
                      <div className="thumbs">
                        <i className={this.state.rating == -1 ? dislikeOn : dislikeOff}></i>
                        &nbsp;
                        <i className={this.state.rating == 1 ? likeOn : likeOff}></i>
                      </div>
                      <div className="message2">
                        <p>
                          Utilize as setas do comando (esquerda e direita)
                          <br /> e use o <b>OK</b> para confirmar.
                        </p>
                        <p>
                          A retomar a emissão em <b>{this.state.timer}</b> segundos
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
        );
    };

    /**
    * adds key listeners
    * sets timer for the rating screen to dissapear
    * if the timer reaches 0 and nothing was chosen then rating is 0 automatically
    */
    componentDidMount() {
        document.addEventListener('keyup', this._handleKey);

        this.intervalRating = setInterval(()=>{
            this.setState({timer:this.state.timer-1},()=>{
                if(this.state.timer == 0){
                    clearInterval(this.intervalRating);
                    this.props.actionCreators.ratings({rating:0, informative_video_id:this.videoID});
                    this.close(false);
                }
            });
        },1000);
    }

    /**
    * removes key listeners and clears interval
    */
    componentWillUnmount() {
        document.removeEventListener('keyup', this._handleKey);
        clearInterval(this.intervalRating);
    };

    /**
    * key listeners for left and right and pressing enter
    * if enter is pressed than the rating action is fired
    */
    _handleKey(event) {
        let {rating} = this.state;
        if (event.keyCode === 37) {
            this.setState({rating: -1});
        } else if (event.keyCode === 39) {
            this.setState({rating: 1});
        } else if (event.keyCode === 13) {
            this.props.actionCreators.ratings({rating:rating, informative_video_id:this.videoID});
            this.close(true);
        }
    }
};
