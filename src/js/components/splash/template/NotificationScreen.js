'use strict';

import React from 'react';

/**
* NotificationScreen
* When videos where too big a notification used to appear asking people if they wished to continue watching the video
*/
export default class NotificationScreen extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      timer: 10
    };
  }

  render() {
    return (
      <div>
        <div className="timer">{this.state.timer}</div>
        <div>
          Se quer continuar a ver o vídeo carregue em OK
          <div className="ok">
            <img src={require("../../../../assets/images/icons/control_small_a.png")}/>
          </div>
        </div>
      </div>
    );
  };

/**
* Starts the timer when the component is mounted
*/
  componentDidMount() {
    setInterval(function () {
      this.setState({timer: this.state.timer - 1});
    }.bind(this), 1000);
  }

};

