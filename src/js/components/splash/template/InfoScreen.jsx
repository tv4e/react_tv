'use strict';

import React from 'react';
import {Row, Column, Icon, Button} from 'react-foundation';
import _ from 'lodash';

/**
* Splash Screen that contextualizes the user
* shows time/ date
* shows taxi and pharmacy number
* shows weather
*/
export default class SplashScreen extends React.Component {

  constructor(props) {
    super(props);

  }

  render() {
    let month = new Date().getMonth();
    let year = new Date().getFullYear();
    let day = new Date().getDay();
    let dayNr = new Date().getDate();
    let hour = new Date().getHours();
    let minute = new Date().getMinutes().toString().length == 1 ? `0${new Date().getMinutes()}` : `${new Date().getMinutes()}`;
    let monthsPT = ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'];
    const weatherIcons = {
      '01': {
        day: <img src={require("../../../../assets/images/icons/weather/01d.png")}/>,
        night: <img src={require("../../../../assets/images/icons/weather/01n.png")}/>,
        backImg: <img className="backgroundImage" src={require("../../../../assets/images/weather/clear sky-min.jpg")}/>
      },
      '02': {
        day: <img src={require("../../../../assets/images/icons/weather/02d.png")}/>,
        night: <img src={require("../../../../assets/images/icons/weather/02n.png")}/>,
        backImg: <img className="backgroundImage"
                      src={require("../../../../assets/images/weather/few clouds-min.jpg")}/>
      },
      '03': {
        day: <img src={require("../../../../assets/images/icons/weather/03d.png")}/>,
        night: <img src={require("../../../../assets/images/icons/weather/03n.png")}/>,
        backImg: <img className="backgroundImage"
                      src={require("../../../../assets/images/weather/scattered clouds-min.jpeg")}/>
      },
      '04': {
        day: <img src={require("../../../../assets/images/icons/weather/04d.png")}/>,
        night: <img src={require("../../../../assets/images/icons/weather/04n.png")}/>,
        backImg: <img className="backgroundImage"
                      src={require("../../../../assets/images/weather/broken clouds-min.jpeg")}/>
      },
      '09': {
        day: <img src={require("../../../../assets/images/icons/weather/09d.png")}/>,
        night: <img src={require("../../../../assets/images/icons/weather/09n.png")}/>,
        backImg: <img className="backgroundImage" src={require("../../../../assets/images/weather/rain-min.jpeg")}/>
      },
      '10': {
        day: <img src={require("../../../../assets/images/icons/weather/10d.png")}/>,
        night: <img src={require("../../../../assets/images/icons/weather/10n.png")}/>,
        backImg: <img className="backgroundImage" src={require("../../../../assets/images/weather/rain-min.jpeg")}/>
      },
      '11': {
        day: <img src={require("../../../../assets/images/icons/weather/11d.png")}/>,
        night: <img src={require("../../../../assets/images/icons/weather/11n.png")}/>,
        backImg: <img className="backgroundImage"
                      src={require("../../../../assets/images/weather/thunderstorm-min.jpg")}/>
      },
      '13': {
        day: <img src={require("../../../../assets/images/icons/weather/13d.png")}/>,
        night: <img src={require("../../../../assets/images/icons/weather/13n.png")}/>,
        backImg: <img className="backgroundImage" src={require("../../../../assets/images/weather/snow-min.jpg")}/>
      },
      '50': {
        day: <img src={require("../../../../assets/images/icons/weather/04d.png")}/>,
        night: <img src={require("../../../../assets/images/icons/weather/04n.png")}/>,
        backImg: <img className="backgroundImage" src={require("../../../../assets/images/weather/mist-min.jpeg")}/>
      }
    };
    let weekday = ['Domingo', '2ª feira', '3ª feira', '4ª feira', '5ª feira', '6ª feira', 'Sábado'];

    let temp = this.props.weather.items.cod == 200 ? this.props.weather.items.main.temp : '';
    let iconCode = this.props.weather.items.cod == 200 ? this.props.weather.items.weather[0].icon : '';
    let iconCodeParsed = iconCode.slice(0, -1);
    let icon = this.props.weather.items.cod == 200 ?
      hour > 6 && hour < 20 ? weatherIcons[iconCodeParsed].day : weatherIcons[iconCodeParsed].night : null;
    let image = this.props.weather.items.cod == 200 ? weatherIcons[iconCodeParsed].backImg : null;

    let message = 'Bom dia';
    if (hour >= 12 && hour < 20) {
        message = 'Boa tarde';
    } else if (hour <= 6 || hour >= 20) {
        message = 'Boa noite';
    }

    let farmName = !_.isEmpty(this.props.farmacias.items) ? this.props.farmacias.items.result.name : '';
    let farmNum = !_.isEmpty(this.props.farmacias.items) ? this.props.farmacias.items.result.formatted_phone_number : '';
    let taxiNum = !_.isEmpty(this.props.taxis.items) ? this.props.taxis.items.result.formatted_phone_number : '';

    return (
      <div className="infoScreen">
        {image}
        <div className="backgroundColor"/>

        <Row>
          <Column large={6} className="weather">
            {icon}
            {parseInt(temp)}º
          </Column>
          <Column large={6} className="time">{hour}:{minute}</Column>
        </Row>
        <div className="welcome">
          <p>{message}!</p>
          <p>Hoje é {weekday[day]}, {dayNr} de {monthsPT[month]} de {year}</p>
          <p>Estamos {this._getSeason()}</p>
          <br/>
        </div>

        <div className="services">
          <Row>
            <Column large={6}>
              <div className="taxi">
                <p>Serviço de Táxi</p>
                <p>{taxiNum}</p>
              </div>
            </Column>
            <Column large={6}>
              <div className="drugs">
                <p>{
                  _.truncate(farmName, {
                    'length': 30,
                    'separator': /,? +/
                  })}
                </p>
                <p>{farmNum}</p>
              </div>
            </Column>
          </Row>
        </div>

      </div>
    );
  };


  /** 
  * Get correct Season according to the year - thank you random stackoverflow guy !
  */
  _getSeason() {
    Date.fromJulian = function (j) {
      j = (+j) + (30.0 / (24 * 60 * 60));
      var A = Date.julianArray(j, true);
      return new Date(Date.UTC.apply(Date, A));
    };
    Date.julianArray = function (j, n) {
      var F = Math.floor;
      var j2, JA, a, b, c, d, e, f, g, h, z;
      j += .5;
      j2 = (j - F(j)) * 86400.0;
      z = F(j);
      f = j - z;
      if (z < 2299161) a = z;
      else {
        g = F((z - 1867216.25) / 36524.25);
        a = z + 1 + g - F(g / 4);
      }
      b = a + 1524;
      c = F((b - 122.1) / 365.25);
      d = F(365.25 * c);
      e = F((b - d) / 30.6001);
      h = F((e < 14) ? (e - 1) : (e - 13));
      var JA = [F((h > 2) ? (c - 4716) : (c - 4715)),
        h - 1, F(b - d - F(30.6001 * e) + f)];
      var JB = [F(j2 / 3600), F((j2 / 60) % 60), Math.round(j2 % 60)];
      JA = JA.concat(JB);
      if (typeof n == 'number') return JA.slice(0, n);
      return JA;
    };
    Date.getSeasons = function (y, wch) {
      y = y || new Date().getFullYear();
      if (y < 1000 || y > 3000) throw y + ' is out of range';
      var Y1 = (y - 2000) / 1000, Y2 = Y1 * Y1, Y3 = Y2 * Y1, Y4 = Y3 * Y1;
      var jd, t, w, d, est = 0, i = 0, Cos = Math.degCos, A = [y],
        e1 = [485, 203, 199, 182, 156, 136, 77, 74, 70, 58, 52, 50, 45, 44, 29, 18, 17, 16, 14, 12, 12, 12, 9, 8],
        e2 = [324.96, 337.23, 342.08, 27.85, 73.14, 171.52, 222.54, 296.72, 243.58, 119.81, 297.17, 21.02,
          247.54, 325.15, 60.93, 155.12, 288.79, 198.04, 199.76, 95.39, 287.11, 320.81, 227.73, 15.45],
        e3 = [1934.136, 32964.467, 20.186, 445267.112, 45036.886, 22518.443,
          65928.934, 3034.906, 9037.513, 33718.147, 150.678, 2281.226,
          29929.562, 31555.956, 4443.417, 67555.328, 4562.452, 62894.029,
          31436.921, 14577.848, 31931.756, 34777.259, 1222.114, 16859.074];
      while (i < 4) {
        switch (i) {
          case 0:
            jd = 2451623.80984 + 365242.37404 * Y1 + 0.05169 * Y2 - 0.00411 * Y3 - 0.00057 * Y4;
            break;
          case 1:
            jd = 2451716.56767 + 365241.62603 * Y1 + 0.00325 * Y2 + 0.00888 * Y3 - 0.00030 * Y4;
            break;
          case 2:
            jd = 2451810.21715 + 365242.01767 * Y1 - 0.11575 * Y2 + 0.00337 * Y3 + 0.00078 * Y4;
            break;
          case 3:
            jd = 2451900.05952 + 365242.74049 * Y1 - 0.06223 * Y2 - 0.00823 * Y3 + 0.00032 * Y4;
            break;
        }
        var t = (jd - 2451545.0) / 36525,
          w = 35999.373 * t - 2.47,
          d = 1 + 0.0334 * Cos(w) + 0.0007 * Cos(2 * w);
        est = 0;
        for (var n = 0; n < 24; n++) {
          est += e1[n] * Cos(e2[n] + (e3[n] * t));
        }
        jd += (0.00001 * est) / d;
        A[++i] = Date.fromJulian(jd);
      }
      return wch && A[wch] ? A[wch] : A;
    };

    Math.degRad = function (d) {
      return (d * Math.PI) / 180.0;
    };
    Math.degSin = function (d) {
      return Math.sin(Math.degRad(d));
    };
    Math.degCos = function (d) {
      return Math.cos(Math.degRad(d));
    };

    let mine = Date.getSeasons();
    let today = new Date(); //Date.parse('Mar 21, 2014'); //use Date.parse() to check dates other than today
    let firstSpring = mine[1];
    let firstSummer = mine[2];
    let firstFall = mine[3];
    let firstWinter = mine[4];
    let season = '';

    if (today >= firstSpring && today < firstSummer) {
      season = 'na Primavera';
    } else if (today >= firstSummer && today < firstFall) {
      season = 'no Verão';
    } else if (today >= firstFall && today < firstWinter) {
      season = 'no Outono';
    } else if (today >= firstWinter || today < firstSpring) {
      season = 'no Inverno';
    }
    return season;
  }
};

