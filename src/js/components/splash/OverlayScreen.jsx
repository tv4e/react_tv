import React from 'react';
import Splash from './template/SplashScreen';
import Info from './template/InfoScreen';
import Notification from './template/NotificationScreen';
import NotificationAlt from './template/NotificationScreenAlt';
import Rating from './../../containers/Rating';

export default class OverlayScreen extends React.Component {

  /**
  * Splash Overlay Screen 
  * Calls information on Weather/Pharmacy/Taxis
  */
  componentWillMount() {
    this.props.actionCreators.weather();
    this.props.actionCreators.farmacias();
    this.props.actionCreators.taxis();
  }

  componentWillReceiveProps(nextProps) {
    this.props = nextProps;
  }

  render() {
    return (
      <section className="overlay">
        <div className="overlay__display">
          {this._getScreen()}
        </div>
      </section>
    );
  };

  /**
  * selects the overlay screen
  */
  _getScreen() {
    let {videoID, closeRating} = this.props;

    switch (this.props.screen) {
      case 'splash':
        return <Splash/>;
      case 'info':
        return <Info
          weather={this.props.actionData.weather}
          farmacias={this.props.actionData.farmacias}
          taxis={this.props.actionData.taxis}
        />;
      case 'noti':
        return <Notification/>;
      case 'alt':
        return <NotificationAlt/>;
      case 'rating':
        return <Rating videoID = {videoID} closeRating={closeRating}/>;
      default:
        return null;
    }
  }
};

/**
* a parameter to choose which sceen to set
*/
OverlayScreen.propTypes = {
  screen: React.PropTypes.string
};

OverlayScreen.defaultProps = {};