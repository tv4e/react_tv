/**
* List of all existing channels
*/
export const channels = [
  "http://rtp-pull-live-h1bnxvdk2-proprtp-app-wrtp.hls.adaptive.level3.net/liverepeater/smil:rtp1.smil/playlist.m3u8",
  "http://rtp-pull-live-h1bnxvdk2-proprtp-app-wrtp.hls.adaptive.level3.net/liverepeater/smil:rtp2.smil/playlist.m3u8",
  "http://live.impresa.pt/live/sic/sic540p.m3u8",
  "http://iptv.atnog.org/hls/tvi.m3u8",
  "http://live.impresa.pt/live/sicnot/sicnot540p.m3u8",
  "http://rtp-pull-live-h1bnxvdk2-proprtp-app-wrtp.hls.adaptive.level3.net/liverepeater/smil:rtpn.smil/playlist.m3u8",
  "http://rtp-pull-live-h1bnxvdk2-proprtp-app-wrtp.hls.adaptive.level3.net/liverepeater/smil:rtpmem.smil/playlist.m3u8",
  "http://a3live-lh.akamaihd.net/i/lasexta_1@35272/master.m3u8",
  "http://a3live-lh.akamaihd.net/i/antena3_1@35248/master.m3u8"
];