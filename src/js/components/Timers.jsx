/**
* List of all existing timers
*/

export const timers_areYouThere = 60000*110;
export const timers_areYouThere2 = 60000*2;

export const timers_splash = 30000;

export const timers_cron = 60000 * 30;

//.....................................................
export const timers_areYouThere_test = 60000*2;
export const timers_areYouThere2_test = 30000;

export const timers_cron_test = 60000;

export const timers_splash_test = 3000;