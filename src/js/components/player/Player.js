'use strict';

import React from 'react';
import ClapprPlayer from './ClapprPlayer';

/**
 * Player - Parent Component
 * Player parent component wich passes the player porps components and conencts callbacks 
 */ 
export default class Player extends React.Component {
  constructor(props) {
    super(props);
  }

  shouldComponentUpdate(nextProps, nextState) {
    this.props = nextProps;
    return true;
  }

  render() {
    return (
      <ClapprPlayer width="100%" height="100%"
                    autoPlay={this.props.autoPlay}
                    controls={this.props.controls}
                    pause={this.props.pause}
                    stop={this.props.stop}
                    source={this.props.source}
                    onTimeUpdate={this.props.onTimeUpdate}
                    onPause={this.props.onPause}
                    onEnded={this.props.onEnded}
      />
    );
  };


};

Player.propTypes = {
  source: React.PropTypes.string,
  autoPlay: React.PropTypes.bool,
  controls: React.PropTypes.bool,
  pause: React.PropTypes.bool,
  onTimeUpdate: React.PropTypes.func,
  onPause: React.PropTypes.func,
  onEnded: React.PropTypes.func
};

Player.defaultProps = {
  autoPlay: false,
  controls: false,
  pause: false,
  stop: null,
  onTimeUpdate: null,
  onPause:null,
  onEnded: null
};

