import React from 'react';
import Player from './player/Player';
import Auth from '../auth';

import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import Notification from './Notification/Notification';
import Modal from './Modal/Modal';
import OverlayScreen from './../containers/OverlayScreen';
import _ from 'lodash';
import Screenfull from 'screenfull';

import {timers_areYouThere,timers_areYouThere2,timers_cron,timers_splash} from './Timers';
import {channels} from './Channels';


/**
 * iTV - Notification videos
 * This component is the main iTV component which connects all the other platform features
 */
export default class Iptv extends React.Component {

  /**
  * sets the initial platform states
  */
  constructor(props) {
    super(props);
    this._handleKey = this._handleKey.bind(this);

    this.canClick = true;
    this.areYouThereTimer = null;
    this.areYouThereTimer2 = null;
    this.cronTimer = null;

    this.state = {
      channel: 0,
      advertisement: false,
      adVideo: null,
      notification: true,
      notificationThumb: 'ch_1.png',
      notificationType: 'channel',
      notificationChannel: '',
      pauseAd: false,
      pauseTv: true,
      overlaySplash: true,
      seeAd: false,
      overlay: 'splash',
      stopTv:null
    };

    this.adTime = 0;
    this.modalUnpause = false;

    // if(!(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent))){
    //     this.state.pauseTv = false;
    //     this.state.overlaySplash = false;
    // }
  }

  /**
  * Called first everytime the component is accesesed, for instance when the app start or when the videoLibrary is exited
  * Calls initial actions to set channels and boot
  * If the box is ON the channel is get from the app storage(STORE)
  * If the box was OFF the channel is get from the database storage(API - HTTP request)
  */
  componentWillMount() {

    if (this.props.actionData.bootState.state) {
      //SE JÁ ESTIVER LIGADO BUSCA CANAL À STORE QUE GUARDA CANAL
      this.state.notificationThumb = `ch_${this.props.actionData.channelState.items + 1}.png`;
      this.state.notificationChannel = this.props.actionData.channelState.items + 1;
      this.state.channel = this.props.channels[this.props.actionData.channelState.items];
      this.state.pauseTv = false;
      this.state.overlaySplash = false;
      this._setModalTimeout();

    } else {
      //SE ESTIVER A LIGAR BUSCA CANAL DA BASE DE DADOS
      let channel = Auth.getBox().last_channel;
      this.state.channel = this.props.channels[channel - 1];
      this.state.notificationThumb = `ch_${channel}.png`;
      this.state.notificationChannel = channel;

      this.props.actionCreators.setChannel(channel - 1);
      this.props.actionCreators.setBoot(true);
    }
  };

  /**
  * Inherits props fro the Redux store, mainly those resulting from actionCreators 
  * (ActionData - payloads of information which come from HTTP requests )
  * Creates actions (ActionCreators - HTTP requests) which come from the SOCKET connections, in this case the videos which are
  * called from the app using backspace button
  */
  componentWillReceiveProps(nextProps) {
    if (!_.isEmpty(nextProps.actionData.socket.SendVideos) && (!_.isEqual(this.props.actionData.socket.SendVideos, nextProps.actionData.socket.SendVideos))) {
      console.log('RECEIVED VIDEO', nextProps.actionData.socket.SendVideos.informative_video_id);

      this.props.actionCreators.log({
        event: `RECEIVED VIDEO`,
        informative_video_id: nextProps.actionData.socket.SendVideos.informative_video_id
      });
      if (!this.state.advertisement) {
        this._setAdNotification(nextProps.actionData.socket.SendVideos);
      }
    }

    //RETOMAR EMISSÃO TELEVISÃO DELISGA/LIGA OU ON/OFF
    if (!_.isEmpty(nextProps.actionData.socket.SendHdmiState) && (!_.isEqual(this.props.actionData.socket.SendHdmiState, nextProps.actionData.socket.SendHdmiState))) {
      if (nextProps.actionData.socket.SendHdmiState.on_state == 1) {

        if (this.areYouThereTimer != 0) {
          this.state.overlay = 'info';
          this.state.overlaySplash = true;
          this.state.pauseTv = true;
          setTimeout(function () {
            this.setState({overlaySplash: false, pauseTv: false}, () => {

              this._setModalTimeout();

              this._setChannelNotification(this.state.notificationChannel);
            });
          }.bind(this), timers_splash);
        }

      }
    }

    if (!_.isEmpty(nextProps.actionData.socket.SendKey)) {
      this._handleKey({keyCode:parseInt(nextProps.actionData.socket.SendKey.key)});
    }

    this.props = nextProps;
  };

  /**
  * Before rendering some variables are set like:
  *    - The videoID which comes from a socket connection 
  *    - Advertisement - if a advertisement state is true
  *    - Notification - if a notification state is true
  * If the splash screen is set it is shown as an overlay over the ipTV
  * The Modal is fired when the set modalTimeout is fired. The ref makes sure it can be fired anywhere in the code
  */
  render() {
    let videoID = null;
    if(this.props.actionData.socket.SendVideos!==undefined){
      videoID = this.props.actionData.socket.SendVideos.informative_video_id;
    }

    let adVideo = this.state.adVideo;

    let notTitle = `${!_.isEmpty(adVideo) ? adVideo.title : ''}`;
    let notImage = !_.isEmpty(adVideo) ? require(`../../assets/images/asgies/resized3/${adVideo.image}`) : '';
    let ad = !_.isEmpty(adVideo) ? adVideo.url : '';

    let notification = (
      this.state.notification == true && !this.state.overlaySplash &&
      <Notification
        onClose={this._closeNot.bind(this)}
        description={notTitle}
        asgieImage={notImage}
        thumbnail={this.state.notificationThumb}
        channel={this.state.notificationChannel}
        type={this.state.notificationType}
      />
    );

    let advertisement = (this.state.advertisement &&
      <ReactCSSTransitionGroup
        transitionName="advertisement_in"
        transitionAppear={true} transitionAppearTimeout={2000}
        transitionEnter={false} transitionLeave={false}
      >
        <Player
          source={ad}
          pause={this.state.pauseAd}
          autoPlay={true}
          onTimeUpdate={this._onTimeUpdate.bind(this)}
        />
      </ReactCSSTransitionGroup>
    );

    return (
      <div className="mainLayout-wrapper">

        {this.state.overlaySplash &&
          <OverlayScreen screen={this.state.overlay}
                         closeRating={(timeout) => this._closeRating(timeout)}
                         videoID={videoID}/>
        }

        <section className="iptv">
          <Modal
            modal="CheckState"
            ref="modal"
          />
          {notification}
          <div className="iptv__player">
            <Player source={this.state.channel}
                    autoPlay={true}
                    pause={this.state.pauseTv}
                    onPause={this._onPause.bind(this)}
                    stop={this.state.stopTv}
            />
            <div className="iptv__player-overlay">
              {advertisement}
            </div>
          </div>
        </section>
      </div>
    );
  };

  /**
  * After the component is mounted a cron to send videos is set
  * the key listeners is set to listen for inputs
  * send and appReady action throught the socket so that the SET TOP BOX knows when to go FULL SCREEN 
  */
  componentDidMount() {
    this._setCron();

    document.addEventListener('keyup', this._handleKey);

    if (this.state.overlay == 'splash') {
      this.props.actionCreators.socketEmit({on_state: 1}, 'AppReady');

      this._setChannelNotification(this.state.notificationChannel);
    }
  }

  /**
  * When the component Unmounts remove all listeners and clears all timeouts
  */
  componentWillUnmount() {
    document.removeEventListener('keyup', this._handleKey);
    window.clearTimeout(this.areYouThereTimer);
    window.clearTimeout(this.areYouThereTimer2);
    window.clearTimeout(this.cronTimer);
  };

  /**
  * Sets the 2 timer for the AreUThere Modal and fires the modal, as well as updates states
  */
  _setModalTimeout() {
    window.clearTimeout(this.areYouThereTimer);
    window.clearTimeout(this.areYouThereTimer2);
    this.props.actionCreators.eventlessTvState({on_state: 1});
    this.canClick=true;

    if (!this.state.advertisement){
      this.areYouThereTimer = window.setTimeout(()=>{
        this.areYouThereTimer = 0;
        this.canClick=false;

        this.props.actionCreators.log({
          event: "SLEEP_1"
        });

        this.refs.modal._showModal();
        this.props.actionCreators.eventlessTvState({on_state: 0});

        this.areYouThereTimer2 = window.setTimeout(() => {
          this.setState({stopTv:true});
          this.areYouThereTimer2 = 0;

          this.props.actionCreators.log({
            event: "SLEEP_2"
          });
        }, timers_areYouThere2);
      }, timers_areYouThere);
    }
  }

  /**
  * Sets the cron to automatically receive videos
  */
  _setCron() {
    window.clearTimeout(this.cronTimer);
    this.cronTimer = window.setTimeout(() => {
      if (!this.refs.modal.state.componentRendered) {
        if (!this.state.advertisement && !this.state.overlaySplash) {
          this.props.actionCreators.cron();
        }
      }
      this._setCron();
    }, timers_cron);
  }


  /**
  * Opens and advertisment and updates the necessary states
  * Pauses the iTV
  * Hides notifications
  * Set advertisement to true
  * resets timeouts
  */
  _openAdvertisement() {
    this.setState(
      {
        pauseTv: true,
        notification: false,
        advertisement: true,
      }, ()=>{
        this._setModalTimeout();
      }
    );
  }

  /**
  * Closes a notification and in the case of a new informative video notification it sends an action to
  * create a REJECTED VIDEO log 
  */
  _closeNot(type) {
    console.log(type, "Closing notification");
    if (type == 'advertisement') {
      this.setState({notification: false, notificationType:'channel'},()=>{
        this.props.actionCreators.log({
          event: "REJECTED VIDEO",
          informative_video_id: this.props.actionData.socket.SendVideos.informative_video_id
        });
      });
    } else {
      this.setState({notification: false});
    }
  }

  /**
  * When user closes informative video necessary states are updated
  * Unpauses the iTV
  * 50% chance of showing a rating screen
  * Set splash screen to true
  */
  _closeAdvertisement() {
    let seen = parseInt((100 * this.adTime.current) / this.adTime.total);

    if (seen == 0) {
      seen = 1;
    }

    this.props.actionCreators.advertisement({
      seen: seen,
      informative_video_id: this.props.actionData.socket.SendVideos.informative_video_id
    });

    this.props.actionCreators.log({
      event: "SEEN VIDEO NOTIFICATION",
      informative_video_id: this.props.actionData.socket.SendVideos.informative_video_id,
      seen: seen,
    });

    this.props.actionCreators.log({
      event: "CANCELED VIDEO",
      informative_video_id: this.props.actionData.socket.SendVideos.informative_video_id
    });

    if (Math.random() >= 0.5) {
      this.canClick = false;

      this.setState({
        pauseTv: true,
        overlay: 'rating',
        overlaySplash: true,
        advertisement: false,
        adVideo: null
      });
    } else {
      this.setState({
        advertisement: false
      }, ()=>{
        this.setState({
          pauseTv: false,
          adVideo: null
        });
      });
    }

    this._setModalTimeout();
  }

  /**
  * If informative video as ended necessary states are updated
  * Unpauses the iTV
  * 50% chance of showing a rating screen
  * Set splash screen to true
  */
  _endedAdvertisement() {
    this.props.actionCreators.advertisement({
      seen: 100,
      informative_video_id: this.props.actionData.socket.SendVideos.informative_video_id
    });

    this.props.actionCreators.log({
      event: "SEEN VIDEO NOTIFICATION",
      informative_video_id: this.props.actionData.socket.SendVideos.informative_video_id,
      seen: 100,
    });

    if (Math.random() >= 0.5) {

      this.canClick = false;

      this.setState({
        pauseTv: true,
        overlay: 'rating',
        overlaySplash: true,
        advertisement: false,
        adVideo: null
      });
    } else {
      this.setState({
        advertisement: false,
      }, () => {
        this.setState({
          pauseTv: false,
          adVideo: null
        });
      });
    }
  }

  /**
  * Pauses TV
  */
  _onPause() {
    if (this.state.pauseTv == false) {
      this.setState({pauseTv:true},()=>{
        this.setState({pauseTv:false});
      });
    }
  }

  /**
  * checks the current informative video play time and automatically ends it if total time as been reached
  */
  _onTimeUpdate(e) {
    this.adTime = e;
    if (parseInt(e.current) == parseInt(e.total)) {
      this._endedAdvertisement();
    }
  }


  /**
  * KeyListener for different events and scenearios
  */
  _handleKey(event) {
    if (event.keyCode === 13) {
      if (this.areYouThereTimer != 0) {
        this._setModalTimeout();
      } else if (event.keyCode == 13) {
        this.refs.modal._hideModal();
        if (this.modalUnpause) {
          this.modalUnpause = false;
        }

        this.props.actionCreators.log({
          event: "WAKE_1"
        });

        this.setState({overlaySplash: false, pauseTv: false});

        if (this.areYouThereTimer2 == 0) {
          this.setState({advertisement:false, overlaySplash: true, overlay:  'info', pauseTv:true});

          setTimeout(function () {
            this.props.actionCreators.log({
              event: "WAKE_2"
            });
            this.setState({overlaySplash: false, pauseTv: false, stopTv:false}, () => {
              this._setModalTimeout();
              this._setChannelNotification(this.state.notificationChannel);
            });
          }.bind(this), timers_splash);
        } else {
          this._setModalTimeout();
          this._setChannelNotification(this.state.notificationChannel);
        }
      }
    }

    // // Block inputs if no internet
    // if (!this.props.actionData.internetState.state || (this.areYouThereTimer == 0 && !this.state.advertisement && this.canClick && event.keyCode !== 13)) {
    //   event.preventDefault();
    //   return false;
    // }

    // Block inputs if no internet
    if ( (this.areYouThereTimer ==  0 && !this.state.advertisement && this.canClick && event.keyCode !== 13)) {
      event.preventDefault();
      return false;
    }

    if (this.state.overlaySplash && this.state.overlay === 'splash') {
      if (Screenfull.enabled) {
        this.setState({overlay: 'info', pauseTv: true}, () => {
          setTimeout(function () {
            this.setState({overlaySplash: false, pauseTv: false}, () => {
              this._setChannelNotification(this.state.notificationChannel);

              this._setModalTimeout();

            });
          }.bind(this), timers_splash);
        });

        Screenfull.request();
      }
    }
    // Change channels
    else if (this.canClick == true) {
      this._setModalTimeout();

      if (event.keyCode !== 13 && this.state.notificationType == 'advertisement' && !this.state.advertisement && this.state.notification) {
        this.props.actionCreators.log({
          event: "REJECTED VIDEO",
          informative_video_id: this.props.actionData.socket.SendVideos.informative_video_id
        });
      }

      if (event.keyCode === 38 && !this.state.advertisement ) {
        let i = _.indexOf(this.props.channels, this.state.channel) + 1;

        if (i == _.lastIndexOf(this.props.channels)) {
          i = 1;
        } else {
          i = i + 1;
        }

        this.props.actionCreators.setChannel(i - 1);
        this.props.actionCreators.log({event: `CHANGED CHANNEL KEYUP 38: ${i}`});
        this.props.actionCreators.channel({channel: i});
        // notificação canal
        this.setState({pauseTv: false, channel: this.props.channels[i - 1]}, function () {
          this._setChannelNotification(i);
        });
        this._blockClick(800);
      } else if (event.keyCode === 40 && !this.state.advertisement) {
        let i = _.indexOf(this.props.channels, this.state.channel) + 1;

        if (i == 1) {
          i = _.lastIndexOf(this.props.channels);
        } else {
          i = i - 1;
        }
        this.props.actionCreators.setChannel(i - 1);
        this.props.actionCreators.log({event: `CHANGED CHANNEL KEYDOWN 40: ${i}`});
        this.props.actionCreators.channel({channel: i});
        // notificação canal
        this.setState({pauseTv: false, channel: this.props.channels[i - 1]}, function () {
          this._setChannelNotification(i);
        });
        this._blockClick(800);
      } else if (event.keyCode === 13) {
        if (this.state.advertisement) {
          this._closeAdvertisement();
        } else {
          if (this.state.notification == true && this.state.notificationType !== "channel") {
            this._openAdvertisement();
            this._blockClick(1500);
          }
        }
      } else if (event.keyCode === 48) {
        if (this.state.advertisement) {
          this._closeAdvertisement();
        }
        this.props.actionCreators.log({event: `OPENED LIBRARY KEY 48`});
        this.props.history.push('/library2');
      } else if (event.keyCode === 49) {
        if (this.state.advertisement) {
          this._closeAdvertisement();
        }
        this.props.actionCreators.setChannel(0);
        this.props.actionCreators.log({event: `CHANGED CHANNEL KEYUP 49: ${1}`});
        this.props.actionCreators.channel({channel: 1});
        this.setState({pauseTv: false, channel: this.props.channels[0]}, function () {
          this._setChannelNotification(1);
        });
        this._blockClick(1000);
      } else if (event.keyCode === 50) {
        if (this.state.advertisement) {
          this._closeAdvertisement();
        }
        this.props.actionCreators.setChannel(1);
        this.props.actionCreators.log({event: `CHANGED CHANNEL KEYUP 50: ${2}`});
        this.props.actionCreators.channel({channel: 2});

        this.setState({pauseTv: false, channel: this.props.channels[1]}, function () {
          this._setChannelNotification(2);
        });
        this._blockClick(1000);
      } else if (event.keyCode === 51) {
        if (this.state.advertisement) {
          this._closeAdvertisement();
        }
        this.props.actionCreators.setChannel(2);
        this.props.actionCreators.log({event: `CHANGED CHANNEL KEYUP 51: ${3}`});
        this.props.actionCreators.channel({channel: 3});
        this.setState({pauseTv: false, channel: this.props.channels[2]}, function () {
          this._setChannelNotification(3);
        });
        this._blockClick(1000);
      } else if (event.keyCode === 52) {
        if (this.state.advertisement) {
          this._closeAdvertisement();
        }
        this.props.actionCreators.setChannel(3);
        this.props.actionCreators.log({event: `CHANGED CHANNEL KEYUP 52: ${4}`});
        this.props.actionCreators.channel({channel: 4});
        this.setState({pauseTv: false, channel: this.props.channels[3]}, function () {
          this._setChannelNotification(4);
        });
        this._blockClick(1000);
      } else if (event.keyCode === 53) {
        if (this.state.advertisement) {
          this._closeAdvertisement();
        }
        this.props.actionCreators.setChannel(4);
        this.props.actionCreators.log({event: `CHANGED CHANNEL KEYUP 53: ${5}`});
        this.props.actionCreators.channel({channel: 5});
        this.setState({pauseTv: false, channel: this.props.channels[4]}, function () {
          this._setChannelNotification(5);
        });
        this._blockClick(1000);
      } else if (event.keyCode === 54) {
        if (this.state.advertisement) {
          this._closeAdvertisement();
        }
        this.props.actionCreators.setChannel(5);
        this.props.actionCreators.log({event: `CHANGED CHANNEL KEYUP 54: ${6}`});
        this.props.actionCreators.channel({channel: 6});
        this.setState({pauseTv: false, channel: this.props.channels[5]}, function () {
          this._setChannelNotification(6);
        });
        this._blockClick(1000);
      } else if (event.keyCode === 55) {
        if (this.state.advertisement) {
          this._closeAdvertisement();
        }
        this.props.actionCreators.setChannel(6);
        this.props.actionCreators.log({event: `CHANGED CHANNEL KEYUP 55: ${7}`});
        this.props.actionCreators.channel({channel: 7});
        this.setState({pauseTv: false, channel: this.props.channels[6]}, function () {
          this._setChannelNotification(7);
        });
        this._blockClick(1000);
        // Send video manually
      } else if (event.keyCode === 8) {
        this.props.actionCreators.sendVideo();
        // this.props.actionCreators.log({event:`FORCED VIDEO SEND`});
        this._blockClick(1000);
      } else if (event.keyCode === 56) {
        if (this.state.advertisement) {
          this._closeAdvertisement();
        }
        this.props.actionCreators.setChannel(7);
        this.props.actionCreators.log({event: `CHANGED CHANNEL KEY 56: ${8}`});
        this.props.actionCreators.channel({channel: 8});
        this.setState({pauseTv: false, channel: this.props.channels[7]}, function () {
          this._setChannelNotification(8);
        });
        this._blockClick(1000);
      } else if (event.keyCode === 57) {
        if (this.state.advertisement) {
          this._closeAdvertisement();
        }
        this.props.actionCreators.setChannel(8);
        this.props.actionCreators.log({event: `CHANGED CHANNEL KEYUP 57: ${9}`});
        this.props.actionCreators.channel({channel: 9});
        this.setState({pauseTv: false, channel: this.props.channels[8]}, function () {
          this._setChannelNotification(9);
        });
        this._blockClick(1000);
      }
    }
  };

  /**
  * Configures the notification for the channels
  */
  _setChannelNotification(i) {
    let src = `ch_${i}.png`;

    this.setState({notification:false},()=>{
      this.setState({
        notification: true,
        notificationThumb: src,
        notificationType: "channel",
        notificationChannel: i
      });
    });
  }

  /**
  * Configures the notification for the informative videos
  */
  _setAdNotification(video) {
    this.setState({notification:false},()=>{
      this.setState({
        notification: true,
        notificationType: "advertisement",
        adVideo: video
      });
    });
  }

  /**
  * Blocks KeyListener for an specific time interval
  */
  _blockClick(t) {
    this.canClick = false;
    setTimeout(function () {
      this.canClick = true;
    }.bind(this), t);
  };

  /**
  * Closes the rating menu which as 50% of chance to show up after an informative video and updates states
  */
  _closeRating(timeout) {
    this.canClick = true;

    this.setState({
      pauseTv: false,
      overlaySplash: false
    });

    if (timeout) {
      this._setModalTimeout();
    }
  }
};


Iptv.propTypes = {
  /**
  * channels corresponds to all the valid channels available and is inherited from Channels.jsx
  */
  channels: React.PropTypes.array
};

Iptv.defaultProps = {
  channels
};