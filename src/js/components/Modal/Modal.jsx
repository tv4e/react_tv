import React from 'react';

import BoronModal from 'boron/OutlineModal';
import CheckState from './styles/CheckState';
import InternetOff from './styles/InternetOff';
import Video from './styles/Video';

import {Button, Row, Column, Icon} from 'react-foundation';
import Entities from 'html-entities';

/**
  * Modal - Used to show a modal during the iTV
  * The modal is the parent component which connects to differents Modals (INTERNET STATE, INJECTED VIDEOS, CHECK IF USERS IS WATCHINg TV) which are passed as props. There is a timer that is passed from the parent component that is used as a countdown in certain cases, like the Video.jsx (injected video modal)
*/  
export default class Modal extends React.Component {
  constructor(props) {
    super(props);
    this._handleKey = this._handleKey.bind(this);

    this.state = {
      componentRendered: false
    };
  };

/**
  * Creates a keydown listener 
*/ 
  componentWillMount() {
    document.addEventListener('keydown', this._handleKey);
  };

  render() {
    const components = {
      CheckState,
      InternetOff,
      Video
    };

    const SpecificModal = components[this.props.modal];

  /**
  * The timer prop is used for the video modal and is furtherly explained on that component.
  */

    return (
      <div className={this.props.modal}>
        <BoronModal closeOnClick={false} onHide={this._hideModalCallback.bind(this)}
                    onShow={this._showModalCallback.bind(this)} id="modal" className="modal" ref="modal">
          <SpecificModal timer={this.props.timer} title={this.props.title} image={this.props.image} asgie={this.props.asgie}/>
        </BoronModal>
      </div>
    );
  };

  /**
  * Removes key listener so that it does not conflict with other componenet
  */
  componentWillUnmount() {
    document.removeEventListener('keydown', this._handleKey);
  };

  /**
  * Function to show a modal
  */
  _showModal() {
    this.refs.modal.show();
  };

  /**
  * Function to set componentRendered state to true
  */
  _showModalCallback() {
    this.setState({componentRendered: true});
  };

  /**
  * Function to hide a modal
  */
  _hideModal() {
    this.refs.modal.hide();
    this.props.onClose && this.props.onClose();
  };


  /**
  * Callback when modal is hidden
  */
  _hideModalCallback() {
    // this.props.onHide();
    this.setState({componentRendered: false});
  };

  _handleKey(event) {
    // if(this.state.componentRendered){
    //     if (event.keyCode === 13) {
    //         this._hideModal();
    //     }
    // }
  };
}

Modal.propTypes = {
  /**
  * Select what modal to use
  */
  modal: React.PropTypes.string,
  /**
  * Callback when the modals are closed
  */
  onClose: React.PropTypes.func,
};

Modal.defaultProps = {
  modal: "default",
};
