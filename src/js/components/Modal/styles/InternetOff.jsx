import React from 'react';


/**
  * Modal - InternetOFF Modal
  * This modal shows up when to inform the user there is no internet connection
*/
export default class InternetOff extends React.Component {
  constructor(props) {
    super(props);
  };

  render() {
    return (
      <section className="modal__content">
        <div className="modal__content-top">
          <h1 className="modal__content-top-title">A plataforma encontra-se indisponível</h1>
        </div>
        <div className="modal__content-description">Por favor, aguarde</div>
      </section>
    );
  };

};
