import React from 'react';

import {Button, Row, Column, Icon} from 'react-foundation';

/**
  * Modal - Video Modal
  * This modal shows up when an informative video is called after 30 min. All the props, including the timer are passed from the Modal.jsx. When the timer ends the video starts. If the user presses OK the modal is cancelled and the video not played.
*/
export default class Video extends React.Component {
  constructor(props) {
    super(props);
  };

  render() {
    let image = require(`../../../../assets/images/asgies/resized3/${this.props.image}`);
    return (
      <section className="modalVideo">
        <div className="modal__content">
        <div className="modal__content-top">
        <div className="modal__content-top-thumb">
          <img src={image}/>
        </div>
          <h1 className="modal__content-top-title">{this.props.title}</h1>
        </div>
        <div className="modal__content-description">
          <p>
            Para cancelar pressione &nbsp;
            <img src={require("../../../../assets/images/icons/control_small_a.png")}/>
          </p>
          <p>O vídeo irá começar em <span className="highlight">{this.props.timer}</span> segundos</p>
        </div>
        </div>
      </section>
    );
  };
};

