import React from 'react';

import {Button, Row, Column, Icon} from 'react-foundation';

/**
  * Modal - CheckState Modal
  * The CheckState Modal is used to check if the user is still viewing TV. After a 1 and half timer the modal shows up and the tv is paused. if the user presses ok the tv is unpaused and the modal dissapears, thus resetting the timer
*/
export default class CheckState extends React.Component {
  constructor(props) {
    super(props);
  };

  render() {
    return (
      <section className="modal__content">
        <div className="modal__content-top">
          <h1 className="modal__content-top-title">Ainda aí está?</h1>
        </div>
        <div className="modal__content-description">
          Para continuar a ver televisão pressione &nbsp;
          <img src={require("../../../../assets/images/icons/control_small_a.png")}/>
        </div>
      </section>
    );
  };
};

