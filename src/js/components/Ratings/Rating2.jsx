import React from 'react';
import Auth from '../../auth';

/**
  * Ratings - Thumb up and thumb down based Rating
  * A rating screen that has 50% of chance to appear. This gives inputs to the recommendation engine if the user liked the video
*/  
export default class Rating2 extends React.Component {

  constructor(props) {
    super(props);

    this._handleKey = this._handleKey.bind(this);

    this.intervalRating = "";

    this.state = {
      rating: 0,
      timer:30
    };

  }

  componentWillMount(){
  }

  render() {
    let likeOn = "fa fa-thumbs-up large green";
    let likeOff = "fa fa-thumbs-up large";
    let dislikeOn = "fa fa-thumbs-down red";
    let dislikeOff = "fa fa-thumbs-down";

    return (
      <section className="mainLayout">
        <div className="mainLayout-wrapper ">
          <div className="overlay">
            <div className="splashScreen">
              <div className="rating">
                <div className="message1">
                  <p>
                    Gostaríamos de saber a sua opinião sobre
                    este vídeo para lhe recomendar conteúdos
                    mais adequados aos seus interesses.
                  </p>
                </div>
                <div className="thumbs">
                  <i className={this.state.rating == -1 ? dislikeOn : dislikeOff}></i>
                  &nbsp;
                  <i className={this.state.rating == 1 ? likeOn : likeOff}></i>
                </div>
                <div className="message2">
                  <p>
                    Utilize as setas do comando (esquerda e direita)
                    <br /> e use o <b>OK</b> para confirmar.
                  </p>
                  <p>
                    A retomar a emissão em <b>{this.state.timer}</b> segundos
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  };

  /**
  * Key up event listener. Timer that closes the rating screen returning to the IPTV. When the timer ends and there is no user input a HTTP request is also sent whith a rating 0 for the informative video. 
  */
  componentDidMount() {
    document.addEventListener('keyup', this._handleKey);

    this.intervalRating = setInterval(()=>{
      this.setState({timer:this.state.timer-1},()=>{
        if(this.state.timer == 0){
          clearInterval(this.intervalRating);
          this.props.actionCreators .ratings({rating:0, informative_video_id:this._getURLParameter('informative_video_id')});
          this.props.history.push(`/${ Auth.getIptv()}`);
        }
      });
    },1000);
  }


  /**
  * Removes key listener so that it does not conflict with other component. Removes timer event so that it does not conflict with other component.
  */
  componentWillUnmount() {
    document.removeEventListener('keyup', this._handleKey);
    clearInterval(this.intervalRating);
  };

  /**
  *Key Listener events
  */
  _handleKey(event) {
    let {rating} = this.state;
    /**
    * keycode 37 to thumbs down
    */
    if (event.keyCode === 37) {
      this.setState({rating: -1});
    } else if (event.keyCode === 39) {
   /**  
    * keycode 37 to thumbs up
    */
      this.setState({rating: 1});
    } else if (event.keyCode === 13) {
    /**  
    * keycode 13 to press ok. Send HTTP request with the rating for the informative video and returns to the IPTV.
    */
        this.props.actionCreators .ratings({rating:rating, informative_video_id:this._getURLParameter('informative_video_id')});
        this.props.history.push(`/${ Auth.getIptv()}`);
    }
  }
  _getURLParameter(name) {
    return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search) || [null, ''])[1].replace(/\+/g, '%20')) || null;
  }
};
