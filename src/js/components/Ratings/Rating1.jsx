'use strict';

import React from 'react';
import Auth from '../../auth';

/**
  * Ratings - StarBased Rating
  * A rating screen that has 50% of chance to appear. This gives inputs to the recommendation engine if the user liked the video
*/  
export default class Rating1 extends React.Component {

  constructor(props) {
    super(props);

    this._handleKey = this._handleKey.bind(this);


    this.state = {
      rating: 0
    };

  }


  render() {

    let golden = "fi-star large golden";
    let grey = "fi-star large";

    return (
      <section className="mainLayout">
        <div className="mainLayout-wrapper ">
          <div className="overlay">
            <div className="splashScreen">
              <div className="rating">
                <p>
                  Gostariamos de saber a sua opinião sobre<br></br>
                  este vídeo para lhe recomendar conteúdos<br></br>
                  mais adequados aos seus interesses!
                </p>
                <i className={this.state.rating >= 1 ? golden : grey}></i>
                <i className={this.state.rating >= 2 ? golden : grey}></i>
                <i className={this.state.rating >= 3 ? golden : grey}></i>
                <i className={this.state.rating >= 4 ? golden : grey}></i>
                <i className={this.state.rating >= 5 ? golden : grey}></i>
                <p>
                  Utilize as setas do comando (esquerda e direita)<br></br>
                  para <b>indicar o quanto gostou</b> deste vídeo.<br></br>
                  Use o <b>OK</b> para confirmar.
                </p>
              </div>
            </div>
          </div>
        </div>
      </section>
    );

  };

 /**
  * Key up event listener
  */
  componentDidMount() {
    document.addEventListener('keyup', this._handleKey);
  }

 /**
  * Removes key listener so that it does not conflict with other component
  */
  componentWillUnmount() {
    document.removeEventListener('keyup', this._handleKey);
  };

 /**
  *Key Listener events
  */
  _handleKey(event) {
    let rating;

    /**
    * keycode 37 to remove stars
    */
    if (event.keyCode === 37) {
      rating = this.state.rating - 1;
      if (rating < 0) {
        rating = 0;
      }
      this.setState({rating: rating});
      console.log(this.state.rating);
    } else if (event.keyCode === 39) {
      /**
      * keycode 39 to add stars
      */
      rating = this.state.rating + 1;
      if (rating > 5) {
        rating = 5;
      }
      this.setState({rating: rating});
      console.log(this.state.rating);
    } else if (event.keyCode === 13) {
     /**
      * keycode 13 to return to IPTV
      */
      this.props.history.push(`/${ Auth.getIptv()}`);
    }
  }
};
