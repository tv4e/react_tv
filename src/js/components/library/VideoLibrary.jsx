import React from 'react';
import Header from './modules/Header';
import VideoList from './modules/VideoList';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import Player from '../player/Player';
import _ from 'lodash';
import Modal from './../Modal/Modal';

import Auth from '../../auth';

/**
* Video Library
* 2 row video library, with videos above being the unseen videos and below the seen videos all organized by reception date
*/
export default class VideoLibrary extends React.Component {

  constructor(props) {
    super(props);
    this._handleKey = this._handleKey.bind(this);

    this.canClick = true;
    this.state = {
      seen: false,
      jeto : true,
      lastVideoList: "unseen",
      videoSelected: {
        title: '',
        image: '',
        duration: '',
        url: '',
        id: ''
      },
      changePosition: false,
      advertisement: false,
      pauseAd: false,
    };

    this.adTime = 0;
  }

  componentWillMount() {
    this.props.actionCreators.videos();
  };

  componentWillReceiveProps(nextProps) {
    if (nextProps.actionData.socket.RefreshLib != undefined) {
      nextProps.actionData.videos.items = nextProps.actionData.socket.RefreshLib.videos.original;
    } else {
      nextProps.actionData.videos.items = nextProps.actionData.videos.items;
    }
    this.props = nextProps;

    if (_.isEmpty(this.props.actionData.videos.items.videosUnseen) && !_.isEmpty(this.props.actionData.videos.items.videosSeen)) {
      this.state.seen = true;
      this.state.unseen = false;
      this.state.lastVideoList = "seen";
    }

    if (_.isEmpty(this.props.actionData.videos.items.videosUnseen) && !_.isEmpty(this.props.actionData.videos.items.videosSeen)) {
      this.state.lastVideoList = "seen";
    }

    if (!_.isEmpty(nextProps.actionData.socket.SendKey)) {
      this._handleKey({keyCode:parseInt(nextProps.actionData.socket.SendKey.key)});
    }

    //pass this properties to a fucntion
    if (!nextProps.actionData.internetState.state) {
      this._offLine();
    } else {
      this._onLine();
    }
  };

  render() {
    let advertisement = (this.state.advertisement ?
        <div className="iptv__player">
          <div className="iptv__player-overlay">
            <ReactCSSTransitionGroup
              transitionName="advertisement_in"
              transitionAppear={true} transitionAppearTimeout={2000}
              transitionEnter={false} transitionLeave={false}>
              <Player
                source={this.state.videoSelected.url}
                pause={this.state.pauseAd}
                autoPlay={true}
                onTimeUpdate={this._onTimeUpdate.bind(this)}
              />
            </ReactCSSTransitionGroup>
          </div>
        </div>
        : null
    );

    return (
      <div className="mainLayout-wrapper">
        <Modal
          modal="InternetOff"
          ref="internetModal"
        />

        <section className="iptv">
          {advertisement}
        </section>

        <section className="videoLibrary">
          <div className="header">
            <Header exit={0}/>
          </div>
          <div className="body">
            <div className="videoList">
              <p className="videoListTitle">Vídeos Não Vistos</p>
              {
                !_.isEmpty(this.props.actionData.videos.items.videosUnseen) ?
                  <VideoList isFocused={this.state.unseen}
                             onChange={this._changeList.bind(this)}
                             videos={this.props.actionData.videos.items.videosUnseen}
                             internetState={this.props.actionData.internetState.state}
                             changePosition={this.state.changePosition}
                             cardNumber={4}
                  />
                  :
                  <div className="videoList-noVideo">
                    <img src={require("../../../assets/images/icons/no-video.png")}/>
                  </div>
              }
            </div>

            <div className="videoList">
              <p className="videoListTitle">Vídeos Vistos</p>
              {
                !_.isEmpty(this.props.actionData.videos.items.videosSeen) ?
                  <VideoList isFocused={this.state.seen}
                             onChange={this._changeList.bind(this)}
                             videos={this.props.actionData.videos.items.videosSeen}
                             internetState={this.props.actionData.internetState.state}
                  />
                  :
                  <div className="videoList-noVideo">
                    <img src={require("../../../assets/images/icons/no-video.png")}/>
                  </div>
              }
            </div>
          </div>
        </section>
      </div>
    );
  };

  componentDidMount() {
    document.addEventListener('keyup', this._handleKey);
  }

  componentWillUnmount() {
    document.removeEventListener('keyup', this._handleKey);
  };

  _handleKey(event) {
    if (!this.props.actionData.internetState.state) {
      event.preventDefault();
      return false;
    }

    if (event.keyCode == 40 && this.state.advertisement == false) {
      if (!_.isEmpty(this.props.actionData.videos.items.videosSeen)) {
        this.setState({seen: true, unseen: false, lastVideoList: "seen"});
      }
    } else if (event.keyCode == 38 && this.state.advertisement == false) {
      if (!_.isEmpty(this.props.actionData.videos.items.videosUnseen)) {
        this.setState({seen: false, unseen: true, lastVideoList: "unseen"});
      }
    } else if (event.keyCode == 13 && !_.isEmpty(this.state.videoSelected.url) && this.canClick == true) {
      if (this.state.unseen && !this.state.advertisement) {
        this._blockClick(1500);
      } else if (!this.state.unseen && !this.state.advertisement) {
        this._blockClick(1500);
      }

      if (this.state.advertisement) {
        this._closeAdvertisement();
        this._blockClick(1500);
      } else {
        this.setState({seen: false, unseen: false, advertisement: true});
        this._blockClick(1500);
      }
      // Tecla 0
    } else if (event.keyCode === 48 && this.state.advertisement == false) {
      this.props.actionCreators.log({event: `CLOSED LIBRARY KEY 48`});
      this.props.history.push(`/${ Auth.getIptv()}`);
    }
  }

  _onLine() {
    this.refs.internetModal._hideModal();
  }

  _offLine() {
    // this.setState({advertisement: false});
    this.refs.internetModal._showModal();
  }

  _changeList(event) {
    this.setState({videoSelected: event});
    this.listSound = new Audio('http://cdn.tv4e.pt/audios/switch-list.mp3');
    this.listSound.play();
  }

  _onTimeUpdate(e) {
    this.adTime = e;
    if (parseInt(e.current) == parseInt(e.total)) {
      this._endedAdvertisement();
    }
  }

  _endedAdvertisement() {
    if (this.state.lastVideoList == 'unseen') {
      this.props.actionCreators.log({
        event: "SEEN VIDEO LIBRARY",
        informative_video_id: this.state.videoSelected.id,
        seen: 100
      });
    } else {
      this.props.actionCreators.log({
        event: "REWATCHED VIDEO LIBRARY",
        informative_video_id: this.state.videoSelected.id,
        seen: 100
      });
    }

    let newState = this.state.lastVideoList == "seen" ? {seen: true, advertisement: false} : {
      unseen: true,
      advertisement: false
    };

    this.setState(newState, () => {
      if (this.state.unseen) {

        if (this.state.videoSelected.index > 0) {
          this.setState({changePosition: true}, () => {
            this.setState({changePosition: false});
          });
        }

        this.props.actionCreators.advertisement({
          seen: 100,
          informative_video_id: this.state.videoSelected.id
        });
      }
    });
  }

  _closeAdvertisement() {
    let seen = parseInt((100 * this.adTime.current) / this.adTime.total);

    if (this.state.lastVideoList == 'unseen') {
      this.props.actionCreators.log({
        event: "SEEN VIDEO LIBRARY",
        informative_video_id: this.state.videoSelected.id,
        seen: seen
      });
    } else {
      this.props.actionCreators.log({
        event: "REWATCHED VIDEO LIBRARY",
        informative_video_id: this.state.videoSelected.id,
        seen: seen
      });
    }

    let newState = this.state.lastVideoList == "seen" ? {seen: true, advertisement: false} : {
      unseen: true,
      advertisement: false
    };
    if (seen == 0) {
      seen = 1;
    }

    this.setState(newState, () => {
      if (this.state.unseen) {

        if (this.state.videoSelected.index > 0) {
          this.setState({changePosition: true}, () => {
            this.setState({changePosition: false});
          });
        }

        this.props.actionCreators.advertisement({
          seen: seen,
          informative_video_id: this.state.videoSelected.id
        });
      }
    });
  }

  _blockClick(t) {
    this.canClick = false;
    setTimeout(function () {
      this.canClick = true;
    }.bind(this), t);
  };
};

VideoLibrary.propTypes = {};

VideoLibrary.defaultProps = {};