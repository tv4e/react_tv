import React from 'react';
import Header from './modules/Header';
import VideoList from './modules/VideoList';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import Player from '../player/Player';
import _ from 'lodash';
import Modal from './../Modal/Modal';

import Auth from '../../auth';

/**
* VideoLibrary
* 1 row video library, with videos seen and unseend organized by reception date only
*/
export default class VideoLibrary2 extends React.Component {

  constructor(props) {
    super(props);
    this._handleKey = this._handleKey.bind(this);

    this.canClick = true;
    this.state = {
      videoSelected: {
        title: '',
        image: '',
        duration: '',
        url: '',
        id: '',
        type: ''
      },
      changePosition: false,
      advertisement: false,
      pauseAd: false,
    };

    this.adTime = 0;
  }

  componentWillMount() {
    this.props.actionCreators.videos();
  };

  componentWillReceiveProps(nextProps) {
    if (nextProps.actionData.socket.RefreshLib != undefined) {
      nextProps.actionData.videos.items = nextProps.actionData.socket.RefreshLib.videos.original;
    }

    if (!_.isEmpty(nextProps.actionData.socket.SendKey)) {
      this._handleKey({keyCode:parseInt(nextProps.actionData.socket.SendKey.key)});
    }

    this.props = nextProps;

    //pass this properties to a function
    if (!nextProps.actionData.internetState.state) {
      this._offLine();
      
    } else {
      this._onLine();
    }
  };

  render() {
    let advertisement = (this.state.advertisement ?
        <div className="iptv__player">
          <div className="iptv__player-overlay">
            <ReactCSSTransitionGroup
              transitionName="advertisement_in"
              transitionAppear={true} transitionAppearTimeout={2000}
              transitionEnter={false} transitionLeave={false}>
              <Player
                source={this.state.videoSelected.url}
                pause={this.state.pauseAd}
                autoPlay={true}
                onTimeUpdate={this._onTimeUpdate.bind(this)}
              />
            </ReactCSSTransitionGroup>
          </div>
        </div>
        : null
    );

    let sentVideos = !_.isEmpty(this.props.actionData.videos.items.sentVideos) ? this.props.actionData.videos.items.sentVideos : [];
    let socialVideos = !_.isEmpty(this.props.actionData.videos.items.socialVideos) ? this.props.actionData.videos.items.socialVideos : [];
    this.videos = _.concat(sentVideos, socialVideos);
    this.videos = _.orderBy(this.videos, 'date', 'desc');

    return (
      <div className="mainLayout-wrapper">
        <Modal
          modal="InternetOff"
          ref="internetModal"
        />

        <section className="iptv">
          {advertisement}
        </section>

        <section className="videoLibraryBG">
          <div className="videoLibrary2">
            <div className="header">
              <Header exit={0}/>
            </div>
            <div className="body">
              <div className="videoList">
                {
                  !_.isEmpty(this.videos) ?
                    <VideoList onChange={this._changeList.bind(this)}
                               videos={this.videos}
                               internetState={true}
                               changePosition={this.state.changePosition}
                               cardNumber={2}
                    />
                    :
                    <div className="videoList-noVideo">
                      <img src={require("../../../assets/images/icons/no-video.png")}/>
                    </div>
                }
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  };

  componentDidMount() {
    document.addEventListener('keyup', this._handleKey);
  }

  componentWillUnmount() {
    document.removeEventListener('keyup', this._handleKey);
  };

  _handleKey(event) {
    // if (!this.props.actionData.internetState.state) {
    //   event.preventDefault();
    //   return false;
    // }

    if (event.keyCode == 13 && !_.isEmpty(this.state.videoSelected.url) && this.canClick == true) {
      if (!this.state.advertisement) {
        this._blockClick(1500);
      }

      if (this.state.advertisement) {
        this._closeAdvertisement();
        this._blockClick(1500);
      } else {
        this.setState({advertisement: true});
        this._blockClick(1500);
      }
      // Tecla 0
    } else if (event.keyCode === 48) {
      if (this.state.advertisement) {
        this._closeAdvertisement();
        this._blockClick(1500);
      }
      this.props.actionCreators.log({event: `CLOSED LIBRARY KEY 48`});
      this.props.history.push(`/${Auth.getIptv()}`);
    } else if (event.keyCode === 49) {
      if (this.state.advertisement) {
        this._closeAdvertisement();
        this._blockClick(1500);
      }
      this.props.actionCreators.log({event: `CLOSED LIBRARY KEY 49`});
      this.props.actionCreators.setChannel(1 - 10);
      this.props.history.push(`/${Auth.getIptv()}`);
    } else if (event.keyCode === 50) {
      if (this.state.advertisement) {
        this._closeAdvertisement();
        this._blockClick(1500);
      }
      this.props.actionCreators.log({event: `CLOSED LIBRARY KEY 50`});
      this.props.actionCreators.setChannel(2 - 1);
      this.props.history.push(`/${Auth.getIptv()}`);
    } else if (event.keyCode === 51) {
      if (this.state.advertisement) {
        this._closeAdvertisement();
        this._blockClick(1500);
      }
      this.props.actionCreators.log({event: `CLOSED LIBRARY KEY 51`});
      this.props.actionCreators.setChannel(3 - 1);
      this.props.history.push(`/${Auth.getIptv()}`);
    } else if (event.keyCode === 52) {
      if (this.state.advertisement) {
        this._closeAdvertisement();
        this._blockClick(1500);
      }
      this.props.actionCreators.log({event: `CLOSED LIBRARY KEY 52`});
      this.props.actionCreators.setChannel(4 - 1);
      this.props.history.push(`/${Auth.getIptv()}`);
    } else if (event.keyCode === 53) {
      if (this.state.advertisement) {
        this._closeAdvertisement();
        this._blockClick(1500);
      }
      this.props.actionCreators.log({event: `CLOSED LIBRARY KEY 53`});
      this.props.actionCreators.setChannel(5 - 1);
      this.props.history.push(`/${Auth.getIptv()}`);
    } else if (event.keyCode === 54) {
      if (this.state.advertisement) {
        this._closeAdvertisement();
        this._blockClick(1500);
      }
      this.props.actionCreators.log({event: `CLOSED LIBRARY KEY 54`});
      this.props.actionCreators.setChannel(6 - 1);
      this.props.history.push(`/${Auth.getIptv()}`);
    } else if (event.keyCode === 55) {
      if (this.state.advertisement) {
        this._closeAdvertisement();
        this._blockClick(1500);
      }
      this.props.actionCreators.log({event: `CLOSED LIBRARY KEY 55`});
      this.props.actionCreators.setChannel(7 - 1);
      this.props.history.push(`/${Auth.getIptv()}`);
    } else if (event.keyCode === 56) {
      if (this.state.advertisement) {
        this._closeAdvertisement();
        this._blockClick(1500);
      }
      this.props.actionCreators.log({event: `CLOSED LIBRARY KEY 56`});
      this.props.actionCreators.setChannel(8 - 1);
      this.props.history.push(`/${Auth.getIptv()}`);
    } else if (event.keyCode === 57) {
      if (this.state.advertisement) {
        this._closeAdvertisement();
        this._blockClick(1500);
      }
      this.props.actionCreators.log({event: `CLOSED LIBRARY KEY 57`});
      this.props.actionCreators.setChannel(9 - 1);
      this.props.history.push(`/${Auth.getIptv()}`);
    }
  }

  _onLine() {
    this.refs.internetModal._hideModal();
  }

  _offLine() {
    this.setState({advertisement: false});
    this.refs.internetModal._showModal();
  }

  _changeList(event) {
    this.setState({videoSelected: event});
  }

  _onTimeUpdate(e) {
    this.adTime = e;
    if (parseInt(e.current) == parseInt(e.total)) {
      this._endedAdvertisement();
    }
  }

  _endedAdvertisement() {
    if (this.state.videoSelected.type == 'unseen') {
      this.props.actionCreators.log({
        event: "SEEN VIDEO LIBRARY",
        informative_video_id: this.state.videoSelected.id,
        seen: 100
      });

      if (Math.random() >= 0.5) {
        this.props.history.push(`/rating2?informative_video_id=${this.state.videoSelected.id}`);
      }
    } else if (this.state.videoSelected.type == 'seen') {
      this.props.actionCreators.log({
        event: "REWATCHED VIDEO LIBRARY",
        informative_video_id: this.state.videoSelected.id,
        seen: 100
      });
    }

    this.props.actionCreators.advertisement({
      seen: 100,
      informative_video_id: this.state.videoSelected.id
    });


    this.setState({ advertisement: false});
  }

  _closeAdvertisement() {
    let seen = parseInt((100 * this.adTime.current) / this.adTime.total);
    if (seen == 0) {
      seen = 1;
    }

    if (this.state.videoSelected.type == 'unseen') {
      this.props.actionCreators.log({
        event: "SEEN VIDEO LIBRARY",
        informative_video_id: this.state.videoSelected.id,
        seen: seen
      });

      if (Math.random() >= 0.5) {
        this.props.history.push(`/rating2?informative_video_id=${this.state.videoSelected.id}`);
      }
    } else if (this.state.videoSelected.type == 'seen') {
      this.props.actionCreators.log({
        event: "REWATCHED VIDEO LIBRARY",
        informative_video_id: this.state.videoSelected.id,
        seen: seen
      });
    }

    this.props.actionCreators.advertisement({
      seen: seen,
      asgie_id: this.state.videoSelected.asgie_id,
      informative_video_id: this.state.videoSelected.id
    });


    this.setState({advertisement: false});
  }

  _blockClick(t) {
    this.canClick = false;
    setTimeout(function () {
      this.canClick = true;
    }.bind(this), t);
  };
};

VideoLibrary2.propTypes = {};

VideoLibrary2.defaultProps = {};