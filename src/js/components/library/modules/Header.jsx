import React from 'react';
import {Row, Column, Icon, Button} from 'react-foundation';
import _ from 'lodash';

/**
* VideoLibrary Header
* Explains how to return to the television
*/
export default class Header extends React.Component {

  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    return (
      <Row className="display">
        <Column className="headerTitle" small={8} medium={9} large={9}>BIBLIOTECA DE VÍDEOS</Column>
        <Column className="headerIcons" small={4} medium={3} large={3}>
          <div className="ulHeader">
            <div>Para ver TV pressione a tecla {this.props.exit}
            </div>
          </div>
        </Column>
      </Row>
    );
  };
};

Header.propTypes = {};

Header.defaultProps = {};