import React from 'react';
import Slider from 'react-image-slider';
import {Row, Column, Icon, Button} from 'react-foundation';
import _ from 'lodash';
import Moment from 'moment';

/**
* VideoList
* List of videos which is shown in the video libraries. Composed by cards with the informative videos information.
*/
export default class VideoList extends React.Component {

  constructor(props) {
    super(props);

    this.videos = this.props.videos;

    this.state = {
      selectedCard: 0,
      list: null
    };
  }

 /**
 * On component mounts sends video information to parent component, the video library
 */
  componentWillMount() {
    this.props.onChange(this.videos[0]);
  }

/**
* 
*/
  componentWillReceiveProps(nextProps) {
    if(!_.isEmpty(nextProps) && !_.isEqual(nextProps.videos, this.videos)){
      this.videos = nextProps.videos;
      this.props.onChange(this.videos[0]);
    }
  }

  render() {
    let selectedCard = [];

    this.videos.map((video, key) => {
      let minute = (Math.floor(video.duration / 60)).toString();
      let second = (video.duration - Math.floor(video.duration / 60) * 60).toString();
      minute = minute.length == 1 ? `0${minute}` : minute;
      second = second.length == 1 ? `0${second}` : second;
      video.index = key;

      if (this.state.selectedCard == key) {
        selectedCard.push(
          <div className="selectedCard card" key={key}>
            <div className="card__image">
              <img src={this._getImage(video.image)}/>
              <span className="card__play"><Icon name="fi-play-video large"/></span>
              <div className="card__date">
                {this._getSentDate(video.date)}
              </div>
              <div className="card__time">{`${minute}:${second}`}</div>
            </div>
            <div className="card__title">
              {video.title}
            </div>
          </div>
        );
      } else {
        selectedCard.push(
          <div className="card" key={key}>
            <div className="card__image">
              <img src={this._getImage(video.image)}/>
              <div className="card__date">
                {this._getSentDate(video.date)}
              </div>
              <div className="card__time">{`${minute}:${second}`}</div>
            </div>
            <div className="card__title">
              {video.title}
              </div>
          </div>
        );
      }
    });

    return (
      <Slider
        isFocused={true}
        onPositionChange={this._bla.bind(this)}
        images={this.videos.images}
        changePosition={this.props.changePosition}
        isInfinite={false}
        visibleItems={this.props.cardNumber}
        delay={0}
        scrollToBeginningIfEnd={false}
        skipScrollIfEnd={false}
        disabled={!this.props.internetState}
      >
        {selectedCard}
      </Slider>
    );
  };

  componentDidMount(){
    this.props.onChange(this.videos[0]);
  }
  
  /**
  * On card change sends information to parent component, the video library
  */
  _bla(event) {
    if (this.state.selectedCard != event) {
      this.setState({selectedCard: event});
      this.props.onChange(this.videos[event]);
      this.cardSound = new Audio('http://cdn.tv4e.pt/audios/switch-card.mp3');
      this.cardSound.play();
    }
  }

  _getImage(image) {
    switch (image) {
      case 'transportes.png':
        return require("../../../../assets/images/asgies/resized/transportes.png");
        break;
      case 'social.png':
        return require("../../../../assets/images/asgies/resized/social.png");
        break;
      case 'seguranca.png':
        return require("../../../../assets/images/asgies/resized/seguranca.png");
        break;
      case 'saude.png':
        return require("../../../../assets/images/asgies/resized/saude.png");
        break;
      case 'financas.png':
        return require("../../../../assets/images/asgies/resized/financas.png");
        break;
      case 'cultura.png':
        return require("../../../../assets/images/asgies/resized/cultura.png");
        break;
      case 'autarquicos.png':
        return require("../../../../assets/images/asgies/resized/autarquicos.png");
    }
  }

  /**
  * Structure informative video sent date according to certain rules
  */
  _getSentDate(sent_at) {
    let hours = Moment.duration(Moment(new Date()) - Moment(sent_at)).asSeconds();
    let hoursNow = Moment.duration(Moment(new Date()).format("HH:mm:ss")).asSeconds();
    let sentDate = '';

    if (hours > hoursNow) {
      // 172800 is 48 hours in seconds
      if (Moment.duration(hours-hoursNow, 'seconds').asHours() >= 24) {
        let days = Math.trunc(Moment.duration(hours-hoursNow, 'seconds').asDays())+1;
        sentDate = `Há ${days} dias`;
      } else {
        sentDate = "Ontem";
      }
    } else if (hours < hoursNow) {
      sentDate = "Hoje";
    } else if (hours == hoursNow) {
      sentDate = "Hoje";
    }

    return sentDate;
  }
};

VideoList.propTypes = {
 /**
  * callback for video changed
  */
  onChange: React.PropTypes.func,
 /**
  * array of the informative videos
  */
  videos: React.PropTypes.array
};

VideoList.defaultProps = {};