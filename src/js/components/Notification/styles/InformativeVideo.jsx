'use strict';

import React from 'react';
import _ from 'lodash';
import {Row, Column, Icon, Button} from 'react-foundation';

/**
* Notification Bar - Informative Video Style
* A child component passed to Notification.jsx to define the notification InformativeVideo style to render
*/
export default class InformativeVideo extends React.Component {
  constructor(props) {
    super(props);
  };

  render() {

    let content = !this.props.smallNot ?
      <Row>
        <Column medium={2} large={2} className="asgie_image">
          <img src={this.props.asgieImage}/>
        </Column>

        <Column className="description" medium={6} large={6}>
            <span className="description-asgie">
                {this.props.description}
            </span>
        </Column>

        <Column medium={4} large={4} className="instruction">
          <div>
            Para ver <img src={require("../../../../assets/images/icons/control_small_a.png")}/>
          </div>
        </Column>
      </Row>
      :
      <Row>
        <Column large={6} className="asgie_image">
          <img src={this.props.asgieImage}/>
        </Column>

        <Column large={6} className="instruction">
          <div>
            <img src={require("../../../../assets/images/icons/control_small_a.png")}/>
          </div>
        </Column>
      </Row>;

    return (
      <div className="notification__content-informativeVideo">
        {content}
      </div>
    );
  };


};
InformativeVideo.propTypes = {
  /**
  * The News description
  */
  description: React.PropTypes.string,
  /**
  * The News asgie icon
  */
  asgieImage: React.PropTypes.string,
  /**
  * Boolean to resize the notification after a certain period of time
  */
  smallNot: React.PropTypes.bool
};
