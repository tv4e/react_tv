import React from 'react';

import {Row, Column, Icon, Button} from 'react-foundation';

/**
* Notification Bar - Channel Style
* A child component passed to Notification.jsx to define the notification Channel style to render
*/
export default class Channel extends React.Component {
  constructor(props) {
    super(props);
  };

  render() {
    return (
      <div className="notification__content-channel">
        <Row>
          <Column>
            <div className="notification__content-top">
              <span className="notification__content-top-thumb_c">
                <img src={require(`../../../../assets/images/icons/${this.props.thumbnail}`)}/>
              </span>
            </div>
          </Column>

          <Column className="notification__content-action">
            <div className="notification__content-channel">
              {this.props.channel}
            </div>
          </Column>
        </Row>
      </div>
    );
  };

};
Notification.propTypes = {
  /**
  * The channel's logo
  */
  thumbnail: React.PropTypes.string,
  /**
  * The channel's logo
  */
  channel: React.PropTypes.number
};
