import React from 'react';
import _ from 'lodash';
import {Row, Column, Icon, Button} from 'react-foundation';

/**
* Notification Bar - Informative Video2 Style
* A child component passed to Notification.jsx to define the notification InformativeVideo2 style to render
*/
export default class InformativeVideo2 extends React.Component {
  constructor(props) {
    super(props);
  };

  render() {

    let content = !this.props.smallNot ?
      <Row>
        <Column medium={2} large={2} className="asgie_image">
          <img src={this.props.asgieImage}/>
        </Column>

        <Column className="description" medium={6} large={6}>
          <span className="description-asgie">
            {
              _.truncate(_.upperFirst(_.lowerCase(this.props.description)), {
                'length': 67,
                'separator': /,? +/
              })
            }
          </span>
        </Column>

        <Column medium={4} large={4} className="instruction">
          <div>
            Vai ver o vídeo em
            <br></br>
            {this.props.notificationTimer} segundos
          </div>
        </Column>
      </Row>
      :
      <Row>
        <Column large={6} className="asgie_image">
          <img src={this.props.asgieImage}/>
        </Column>

        <Column large={6} className="instruction">
          <div>
            <img src={require("../../../../assets/images/icons/control_small_a.png")}/>
          </div>
        </Column>
      </Row>;

    return (
      <div className="notification__content-informativeVideo">
        {content}
      </div>
    );
  };
};

InformativeVideo2.propTypes = {
  /**
  * The News description
  */
  description: React.PropTypes.string,
  /**
  * The News asgie icon
  */
  asgieImage: React.PropTypes.string,
  /**
  * Boolean to resize the notification after a certain period of time
  */
  smallNot: React.PropTypes.bool
};