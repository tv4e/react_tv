import React from 'react';

import {Row, Column, Icon, Button} from 'react-foundation';
import Animation from '../tools/Animation';

import Channel from './styles/Channel';
import InformativeVideo from './styles/InformativeVideo';
import InformativeVideo2 from './styles/InformativeVideo2';

/**
* Notification Bar - used to show a notification during the iTV
* The Notification Bar is the parent component which connects to differents styles (CHANNEL, INFORMATIVE VIDEo) which are passed as props.
*/
export default class Notification extends React.Component {

  /**
  * Sets initial states to properly control the component
  */
  constructor(props) {
    super(props);

    this.state = {
      componentRendered: false,
      smallNot: false,
    };

    this.timer = "";
  };

  /**
  * Clears previous timers
  */
  componentWillMount() {
    clearTimeout(this.timer);
  }

  /**
  * Clears previous timers
  */
  componentWillReceiveProps() {
    clearTimeout(this.timer);
  }

  render() {
    let style = this._setStyle();

  /**
  * Animation is a component to easily animate a component. In this case used for the component render state
  */
    return (
      <Animation animation={{
        load: [
          {
            initialStyle: {opacity: "0"},
            style: {opacity: "1"},
            animation: ["fadeIn"],
            priority: 1
          }
        ],
        event: [
          {
            style: {width: "25rem"},
            animation: ["resizeAnim"],
            priority: 1
          }
        ]
      }}
         childId="notification"
         ref="animator">
        <div id="notification" className="notification" ref="notification">
          <section className="notification__content">
            {style}
          </section>
        </div>
      </Animation>
    );
  };

  /**
  * Sets the timer to control the notification (make it small and dissapear)
  */
  componentDidUpdate() {
    if (this.props.type == "channel") {
      clearTimeout(this.timer);
      this.timer = setTimeout(this._hideNotification.bind(this), 3000);
    } else if (this.props.type == "advertisement") {
      clearTimeout(this.timer);
      this.timer = setTimeout(this._hideNotification.bind(this), 30000);
    }
  }

  /**
  * Sets the timer to control the notification (make it small and dissapear)
  */
  componentDidMount() {
    if (this.props.type == "channel") {
      clearTimeout(this.timer);
      this.timer = setTimeout(this._hideNotification.bind(this), 3000);
    } else if (this.props.type == "advertisement") {
      clearTimeout(this.timer);
      this.timer = setTimeout(this._hideNotification.bind(this), 30000);
    }
  }

  /**
  * It is important to clear the timer when the component unmount
  */        
  componentWillUnmount() {
    clearTimeout(this.timer);
  };


  /**
  * Set style selects a child component (style) bases on the prop type
  */
  _setStyle() {
    let content;

    if (this.props.type == "channel") {
      content =
        <Channel
          thumbnail={this.props.thumbnail}
          channel={this.props.channel}
        />;
    } else if (this.props.type == "advertisement") {
      content =
        <InformativeVideo
          description={this.props.description}
          asgieImage={this.props.asgieImage}
          smallNot={this.state.smallNot}
        />;
    } else if (this.props.type == "advertisement2") {
      content =
        <InformativeVideo2
          description={this.props.description}
          asgieImage={this.props.asgieImage}
          smallNot={this.state.smallNot}
          notificationTimer={this.props.notificationTimer}
        />;
    }

    return content;
  }

  /**
  * Called to hide the notification
  */
  _hideNotification() {
    if (this.props.type == "advertisement") {
      this.refs.animator._animate();
      this.setState({smallNot: true});
      setTimeout(() => {
        this.props.onClose('advertisement');
      }, (60000 * 3));
    } else if (this.props.type == "channel") {
         this.props.onClose();
    }
  };

};
Notification.propTypes = {
  /**
  * Used to select a child component (style)
  */
  type: React.PropTypes.string,
  /**
  * Used to write a news title on the notification, passed to the child component
  */
  title: React.PropTypes.string,
  /**
  * Used to write a news description on the notification, passed to the child component
  */
  description: React.PropTypes.string,
  /**
  * Callback when notification is closed
  */
  onClose: React.PropTypes.func,
  /**
  * Callback when notification is opened
  */
  onOpen: React.PropTypes.func,
  /**
  * Used to pass a thumbnail image to the notification
  */
  thumbnail: React.PropTypes.string,
  /**
  * Used to pass an asgie image to the notification
  */
  asgieImage: React.PropTypes.string,
  /**
  * Used to pass the channel number to the notification
  */
  channel: React.PropTypes.number
};
