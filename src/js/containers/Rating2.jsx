import React from 'react';

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import Rating2 from './../components/Ratings/Rating2';
import {withRouter} from 'react-router-dom';

import * as Ratings from './../actions/Ratings';

const mapStateToProps = function (state) {
  return {
    actionData: {
      ratings: {
        items: state.ratingsSuccess,
        hasErrored: state.ratingsHasErrored,
        isLoading: state.ratingsIsLoading
      }
    }
  };
};

const mapDispatchToProps = function (dispatch) {

  let Actions = _.extend({},
    Ratings
  );

  return {actionCreators: bindActionCreators(Actions, dispatch)};
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Rating2));