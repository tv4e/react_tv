import React from 'react';

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';

import VideoLibrary2 from './../components/library/VideoLibrary2';

import * as Profile from './../actions/Profile';
import * as Socket from './../actions/Socket';
import * as Logs from './../actions/Logs';

const mapStateToProps = function (state) {
  return {
    actionData: {
      socket: {
        SendVideos: state.socket.SendVideos,
        RefreshLib: state.socket.RefreshLib,
        SendKey: state.socket.SendKey
      },
      videos: {
        items: state.videosSuccess,
        hasErrored: state.videosHasErrored,
        isLoading: state.videosIsLoading
      },
      internetState: {
        state: state.internetState,
      },
    }
  };
};

const mapDispatchToProps = function (dispatch) {

  let Actions = _.extend({},
    Profile,
    Logs,
    Socket
  );

  return {actionCreators: bindActionCreators(Actions, dispatch)};
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(VideoLibrary2));