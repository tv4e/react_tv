import React from 'react';

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import Root from './../components/Root';
import {withRouter} from 'react-router-dom';

import * as Profile from './../actions/Profile';
import * as Box from './../actions/Box';

const mapStateToProps = function (state) {
  return {
    actionData: {
      internetState: {
        state: state.internetState,
      },
      box: {
        items: state.boxSuccess,
        hasErrored: state.boxHasErrored,
        isLoading: state.boxIsLoading
      },
    }
  };
};

const mapDispatchToProps = function (dispatch) {

  let Actions = _.extend({},
    Profile,
    Box
  );

  return {actionCreators: bindActionCreators(Actions, dispatch)};
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Root));