import React from 'react';

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import Iptv from './../components/Iptv';
import _ from 'lodash';

import {withRouter} from 'react-router-dom';

import * as Profile from './../actions/Profile';
import * as Socket from './../actions/Socket';
import * as Logs from './../actions/Logs';
import * as Weather from './../actions/Weather';
import * as SendVideo from './../actions/SendVideo';
import * as Cron from './../actions/Cron';

const mapStateToProps = function (state) {
  return {
    actionData: {
      socket: {
        SendVideos: state.socket.SendVideos,
        SendHdmiState: state.socket.SendHdmiState,
        SendKey: state.socket.SendKey
      },
      cron: {
        items: state.cronSuccess,
        hasErrored: state.cronHasErrored,
        isLoading: state.cronIsLoading
      },
      profiles: {
        items: state.loginDataSuccess,
        hasErrored: state.loginHasErrored,
        isLoading: state.loginIsLoading
      },
      weather: {
        items: state.weatherSuccess,
        hasErrored: state.weatherHasErrored,
        isLoading: state.weatherIsLoading
      },
      sendVideo: {
        items: state.sendVideoSuccess,
        hasErrored: state.sendVideoHasErrored,
        isLoading: state.sendVideoIsLoading
      },
      bootState: {
        state: state.bootState,
      },
      internetState: {
        state: state.internetState,
      },
      channelState: {
        items: state.channelState,
      }
    }
  };
};

const mapDispatchToProps = function (dispatch) {

  let Actions = _.extend({},
    Profile,
    Logs,
    Socket,
    SendVideo,
    Weather,
    Cron
  );

  return {actionCreators: bindActionCreators(Actions, dispatch)};
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Iptv));