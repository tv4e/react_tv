import React from 'react';

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';

import OverlayScreen from './../components/splash/OverlayScreen';

import * as Socket from './../actions/Socket';
import * as Logs from './../actions/Logs';
import * as Weather from './../actions/Weather';
import * as Farmacias from './../actions/Farmacias';
import * as Taxis from './../actions/Taxis';

const mapStateToProps = function (state) {
  return {
    actionData: {
      socket: {
        SendVideos: state.socket.SendVideos
      },
      weather: {
        items: state.weatherSuccess,
        hasErrored: state.weatherHasErrored,
        isLoading: state.weatherIsLoading
      },
      farmacias: {
        items: state.farmaciaSuccess,
        hasErrored: state.farmaciaHasErrored,
        isLoading: state.farmaciaIsLoading
      },
      taxis: {
        items: state.taxiSuccess,
        hasErrored: state.taxiHasErrored,
        isLoading: state.taxiIsLoading
      },
    }
  };
};

const mapDispatchToProps = function (dispatch) {

  let Actions = _.extend({},
    Logs,
    Socket,
    Weather,
    Farmacias,
    Taxis
  );

  return {actionCreators: bindActionCreators(Actions, dispatch)};
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(OverlayScreen));