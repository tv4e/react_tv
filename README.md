The +TV4E project aims to support the Portuguese elderly to obtain information on public and social services quickly and easily through TV, reducing the probability of an exclusion situation due to information deficits. Regarding this, an iTV platform is under development and it will run in a low-cost Android Set-Top Box (STB) and it uses a regular internet connection. This infrastructure allows access to the same channels that are offered in the Public Digital TV service in Portugal. 

The +TV4E platform provides a splash screen that is presented each time that the user connects the STB. This “welcome” screen, that is active for 30 seconds, aims to guide the user concerning key areas of daily life, providing easy and quick comprehension information which serves to contextualize the users about weather, time, date and current season, as well as contact information for the nearest pharmacy and taxi. Afterwards, when a new information about a certain social or public matter is available, a video spot is created and then injected into the linear television presentation flow. The video can be visualized or not, according to the user preference. The audio-visual content presented to the user is created in an automatic way.
The information presented in the videos is aggregated into seven previously studied macro-areas of interest, namely: (1) health care and welfare services; (2) social services; (3) financial services; (4) culture, informal education and entertainment; (5) security services; (6) local authority services and (7) transport services. 
When the video is sent and accepted by the user, the regular TV broadcast is locally paused and the video is displayed. 

Finally, the linear content is resumed to the exact moment when the emission was interrupted to avoid loosing content. When the video ends, a rating screen is displayed for 25 seconds in which is requested for the the user to express their opinion concerning their interest on the video, through a system of "like" and "dislike". The information gathered at this moment feeds a recommendation system that tune the the content to each STB according to the user preferences. The rating screen only appears 50% of the times the video is viewed, minimizing possible problems in the user experience.

Additionally, all the informative videos sent to the STB are also accessible in a video library feature, always available through pressing “0” key on remote control. In this library, the user can review content already viewed as well as rejected or unseen videos. 

The +TV4E platform also provides two modes of presentation/visualization of the videos, where the user receives an overlaid notification informing that a new video is available and it will start in 15 seconds; or (ii) "notification", where the user only receives a notification, localized at the top of the screen, requesting the user's express wish to start the video display. At this moment, to accept the video, the user should press the “OK” key on the remote control. If the user does not execute any action, after 30 seconds, the notification retracts to the corner of the screen waiting for instruction. After 4 minutes it disappears irreversibly if no action is taken.
# Components


### iTV - Notification videos
`src/js/components/Iptv.jsx`
# 
**Iptv** This component is the main iTV component which connects all the other platform features	

Property | Type | Required | Description
:--- | :--- | :--- | :---
channels|array||channels corresponds to all the valid channels available and is inherited from Channels.jsx

# 
### iTV - Injected videos
`src/js/components/Iptv2.jsx`
#
**Iptv2** This component is the main iTV component which connects all the other platform features

Property | Type | Required | Description
:--- | :--- | :--- | :---
channels|array||channels corresponds to all the valid channels available and is inherited from Channels.jsx

#
### iTV - Developer test
`src/js/components/IptvTeste.jsx`
#
**IptvTeste** This component is the tests iTV component which connects all the other platform features

Property | Type | Required | Description
:--- | :--- | :--- | :---
channels|array||channels corresponds to all the valid channels available and is inherited from Channels.jsx

#
### Modal - Used to show a modal during the iTV
`src/js/components/Modal/Modal.jsx`
#
**Modal** The modal is the parent component which connects to differents Modals (INTERNET STATE, INJECTED VIDEOS, CHECK IF USERS IS WATCHINg TV) which are passed as props. There is a timer that is passed from the parent component that is used as a countdown in certain cases, like the Video.jsx (injected video modal)

Property | Type | Required | Description
:--- | :--- | :--- | :---
modal|string||Select what modal to use
onClose|func||Callback when the modals are closed

#
### Modal - CheckState Modal
`src/js/components/Modal/styles/CheckState.jsx`
#
**CheckState** The CheckState Modal is used to check if the user is still viewing TV. After a 1 and half timer the modal shows up and the tv is paused. if the user presses ok the tv is unpaused and the modal dissapears, thus resetting the timer

### Modal - InternetOFF Modal
`src/js/components/Modal/styles/InternetOff.jsx`
#
**InternetOff** This modal shows up when to inform the user there is no internet connection

### Modal - Video Modal
`src/js/components/Modal/styles/Video.jsx`
#
**Video** This modal shows up when an informative video is called after 30 min. All the props, including the timer are passed from the Modal.jsx. When the timer ends the video starts. If the user presses OK the modal is cancelled and the video not played.

### Notification Bar - used to show a notification during the iTV
`src/js/components/Notification/Notification.jsx`
#
**Notification** The Notification Bar is the parent component which connects to differents styles (CHANNEL, INFORMATIVE VIDEo) which are passed as props.

Property | Type | Required | Description
:--- | :--- | :--- | :---
type|string||Used to select a child component (style)
title|string||Used to write a news title on the notification, passed to the child component
description|string||Used to write a news description on the notification, passed to the child component
onClose|func||Callback when notification is closed
onOpen|func||Callback when notification is opened
thumbnail|string||Used to pass a thumbnail image to the notification
asgieImage|string||Used to pass an asgie image to the notification
channel|number||Used to pass the channel number to the notification

#
### Notification Bar - Channel Style
`src/js/components/Notification/styles/Channel.jsx`
#
**Channel** A child component passed to Notification.jsx to define the notification Channel style to render

### Notification Bar - Informative Video Style
`src/js/components/Notification/styles/InformativeVideo.jsx`
#
**InformativeVideo** A child component passed to Notification.jsx to define the notification InformativeVideo style to render

Property | Type | Required | Description
:--- | :--- | :--- | :---
description|string||The News description
asgieImage|string||The News asgie icon
smallNot|bool||Boolean to resize the notification after a certain period of time

#
### Notification Bar - Informative Video2 Style
`src/js/components/Notification/styles/InformativeVideo2.jsx`
#
**InformativeVideo2** A child component passed to Notification.jsx to define the notification InformativeVideo2 style to render

Property | Type | Required | Description
:--- | :--- | :--- | :---
description|string||The News description
asgieImage|string||The News asgie icon
smallNot|bool||Boolean to resize the notification after a certain period of time

#
### Ratings - StarBased Rating
`src/js/components/Ratings/Rating1.jsx`
#
**Rating1** A rating screen that has 50% of chance to appear. This gives inputs to the recommendation engine if the user liked the video

### Ratings - Thumb up and thumb down based Rating
`src/js/components/Ratings/Rating2.jsx`
#
**Rating2** A rating screen that has 50% of chance to appear. This gives inputs to the recommendation engine if the user liked the video

### Root
`src/js/components/Root.jsx`
#
**Root** This component sets the initial layout after making sure there is internet and sockect connection are stabilished

### Video Library
`src/js/components/library/VideoLibrary.jsx`
#
**VideoLibrary** 2 row video library, with videos above being the unseen videos and below the seen videos all organized by reception date

### VideoLibrary
`src/js/components/library/VideoLibrary2.jsx`
#
**VideoLibrary2** 1 row video library, with videos seen and unseend organized by reception date only


### VideoLibrary Header
`src/js/components/library/modules/Header.jsx`
#
**Header** Explains how to return to the television

### VideoList
`src/js/components/library/modules/VideoList.jsx`
#
**VideoList** List of videos which is shown in the video libraries. Composed by cards with the informative videos information.

Property | Type | Required | Description
:--- | :--- | :--- | :---
onChange|func||callback for video changed
videos|array||array of the informative videos

#
### ClapprPlayer - Functional component
 `src/js/components/player/ClapprPlayer.jsx`
#
This components controls every aspect of the player according to the passes parameters. It is connected to Player.js parent component.

Property | Type | Required | Description
:--- | :--- | :--- | :---
source|string||
watermark|string||
watermarkPosition|string||
autoPlay|bool||
worker|bool||
width|string||
height|string||
controls|bool||
pause|bool||
onTimeUpdate|func||
onPause|func||
onEnded|func||

#
### Player - Parent Component
`src/js/components/player/Player.js`
#
**Player** Player parent component wich passes the player porps components and conencts callbacks

Property | Type | Required | Description
:--- | :--- | :--- | :---
source|string||
autoPlay|bool||
controls|bool||
pause|bool||
onTimeUpdate|func||
onPause|func||
onEnded|func||
stop|||

#
### Splash Overlay Screen 
`src/js/components/splash/OverlayScreen.jsx`
#
**OverlayScreen** 

Property | Type | Required | Description
:--- | :--- | :--- | :---
screen|string||

#
### Splash Screen that contextualizes the user
`src/js/components/splash/template/InfoScreen.jsx`
#
**SplashScreen** shows time/ date
shows taxi and pharmacy number
shows weather

### NotificationScreen
`src/js/components/splash/template/NotificationScreen.js`
#
**NotificationScreen** When videos where too big a notification used to appear asking people if they wished to continue watching the video


### NotificationScreenAlt
`src/js/components/splash/template/NotificationScreenAlt.js`
#
**NotificationScreenAlt** When videos where too big a notification used to appear asking people if they wished to skip watching the video

### Rating screen shown as an overlay
`src/js/components/splash/template/Rating.js`
#
**Rating** As a timer and asks users if they likeed a video showing a thumbs up and a thumbs down
requires the informative video id make a POST request with the rating values

### Initial Loading SplashScreen
`src/js/components/splash/template/SplashScreen.js`
#
**SplashScreen** splash loading screen shown when the set top box is getting prepared to use

###  Animations component
`src/js/components/tools/Animation.jsx`
#
**Animation** This tool offers a simple way of creating animation for pour application. You can set a priority so that elements have an animation ordered.Several elements can have the same priority.List of animation are on the _animations function

Property | Type | Required | Description
:--- | :--- | :--- | :---
animation|object||
styles|object||
childId|string||
